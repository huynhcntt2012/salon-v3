<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use App\Http\Database\themes;
use App\Models\Bills;

class ApiController extends Controller {
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
     /* ----------------------- Category ------------------------------- */
	public function getCategory()
	{
        $data = DB::table('category')->paginate(10);
        return response()->json($data);
	}
    /*
    paramenter
    numerical,name,status,note
    */
    public function createCategory(Request $request)
	{
        // $numerical = $request->input('numerical');
        $numerical = 0;
        $name = $request->input('name');
        // $status = $request->input('status');
        $status = 1;
        $note = $request->input('note');
        $whereInsert = array(
                              'numerical'=>$numerical,
                              'name'=>$name,
                              'status'=>$status,
                              'note'=> $note );
        
        DB::table('category')->insert($whereInsert);
        
        
        $list = DB::table('category')->get();   
        return json_encode($list);
	}
    /**
     * Get detail category
     */
    public function getDetailCategory($id)
    {
        // dd($id);
        $data = DB::table('category')->where('id', $id)->get();
        return json_encode($data);
    }
    /*
    paramenter
    id,numerical,name,status,note
    */
    
    
    public function editCategory($id, Request $request)
	{
        // $id = $request->input('id');
        $numerical = 0;
        $name = $request->input('name');
        $status = 0;
        $note = $request->input('note');
        $whereUpdate = array( 'numerical'=>$numerical,
                              'name'=>$name,
                              'status'=>$status,
                              'note'=> $note );
        
        DB::table('category')->where('id', $id)->update($whereUpdate);
        
        
        $list = DB::table('category')->get();
        return json_encode($list);
	}
    /*
    paramenter
    id
    */
    public function delCategory($id)
	{
        DB::table('category')->where(array('id' => $id))->delete();
        $list = DB::table('category')->get();
        return json_encode($list);
	}
    
    /*  ----------------------- employee money -----------------------*/

    public function getSettingService($id)
	{
        //$list = DB::table('category')->join('sub','category.id','=','sub.category')->select('sub.*','category.name as name1')->get();
        $list = DB::Select('SELECT s.*, s.id as idService,
                            IFNULL((SELECT income FROM income WHERE income.idservice=s.id and income.idemployee = e.id),0) as income,
                            IFNULL((SELECT percent FROM income WHERE income.idservice=s.id and income.idemployee = e.id),0) as percent,
                            e.id,
                            (SELECT category.name FROM category WHERE category.id = s.category) as name1
                            FROM `sub` as s,`employee` as e WHERE e.id = '.$id);
        return json_encode($list);
	}
    
    
    public function getSettingEmployee($id)
	{
        $list = DB::table('employee')->where('phone','=',$id)->get();
        return json_encode($list);
	}
    
    public function setServiceEmployee($idemployee,$idservice,$income,$percent)
	{
        $list = DB::table('income')->where(array('idemployee'=>$idemployee,
                        'idservice'=>$idservice))->get();
                              
        if(count($list) > 0){
            DB::table('income')->where(array('idemployee' => $idemployee, 'idservice'=>$idservice))->update(array('income'=>$income,'percent' => $percent));
        }else{
            $data = array('idemployee'=>$idemployee,
                        'idservice'=>$idservice,
                        'income'=>$income,
                        'percent' => $percent);
            DB::table('income')->insert($data);
        }
	}
    
    /* ----------------------- end employee money ----------------------- */
    
    /* -----------------------End Category ------------------------------- */
    
    
    /* ----------------------- Sub ------------------------------- */
    
    public function getSub()
	{
        
        $list = DB::table('sub')->paginate(3);
        return response()->json($list);
	}
    
    public function getSubAll()
	{
        
        $list = DB::table('category')->get();
        return response()->json($list);
	}

    public function getSingleSub($id) 
    {
        $data = DB::table('sub')->where('category', $id)->paginate(10);
        return response()->json($data);
    }

    /*
    paramenter
    category,name,price,income,note
    */
    public function createSub(Request $request)
	{
	    $category = $request->input('category');
        $name = $request->input('name');
        $price = $request->input('price');
        $income = $request->input('income');
        $note = $request->input('note');
        $whereInsert = array(
                              'category'=>$category,
                              'name'=>$name,
                              'price'=>$price,
                              'income'=> $income,
                              'note' => $note);
        
        DB::table('sub')->insert($whereInsert);
        $list = DB::table('sub')->get();
        return json_encode($list);
	}
    /*
    paramenter
    id,category,name,price,income,note
    */
    public function editSub($id, Request $request)
	{  
        // $id = $request->input('id');
	    $category = $request->input('category');
        $name = $request->input('name');
        $price = $request->input('price');
        $income = $request->input('income');
        $note = $request->input('note');
        $whereUpdate = array(
                              'category'=>$category,
                              'name'=>$name,
                              'price'=>$price,
                              'income'=> $income,
                              'note' => $note);
        
        DB::table('sub')->where('id', $id)->update($whereUpdate);
        $list = DB::table('sub')->get();
        return json_encode($list);
	}
    
    /*
    paramenter
    id
    */
    public function delSub($id)
	{
	   
        // $id = $request->input('id');
        DB::table('sub')->where(array('id' => $id))->delete();
        $list = DB::table('sub')->get();
        return json_encode($list);
	}
    
    /* ----------------------- End Sub ------------------------------- */
    /* ----------------------- Customer ------------------------------- */

    public function getCustomer()
	{
        $list = DB::table('customer')->get();
        return json_encode($list);
    }
    
    public function getSearchPhone(Request $request)
	{
        $list = DB::table('customer')->where('phone','=',$request->input('phone'))->get();
        return json_encode($list);
    }
    
    public function getSearchHigh(Request $request)
	{
        if($request->input('name') == '' && $request->input('phone') != ''){
            $list = DB::table('customer')->where('phone','like','%'.$request->input('phone').'%')->get();
        }else if($request->input('phone') == '' && $request->input('name') != ''){
            $list = DB::table('customer')->where('name','like','%'.$request->input('name').'%')->get();
        }else{
            $list = DB::table('customer')->where('phone','like','%'.$request->input('phone').'%')
                                        ->where('name','like','%'.$request->input('name').'%')->get();
        }
        return json_encode($list);
    }
    
    public function getSearchDate(Request $request)
	{
        if($request->input('date') == ''){
            $list = DB::table('customer')->where('phone','like','%'.$request->input('phone').'%')->get();
        }else {
            $list = DB::table('customer')->where('created_at','like','%'.$request->input('date').'%')->get();
        }
        return json_encode($list);
    }
    
     

    /* ----------------------- End Customer ------------------------------- */
    
    /* ----------------------- link Video  ------------------------------- */
    public function getvideo()
	{
        
        $list = DB::table('videos')->paginate(5);
        return response()->json($list);
	}
    
    public function create(Request $request)
	{
	    $title = $request->input('title');
        $link = $request->input('link_youtube');
        
        $today = date("Y-m-d H:m:s");
        
        $whereInsert = array(
                              'title'=>$title,
                              'link_youtube'=>$link,
                              'creater'=>'admin',
                              'created_at'=> $today);
        
        DB::table('videos')->insert($whereInsert);
        $list = DB::table('videos')->get();
        return json_encode($list);
	}
    public function delvideo($id)
	{
	    DB::table('videos')->where(array('id' => $id))->delete();
        $list = DB::table('videos')->get();
        return json_encode($list);
	}
    
    public function editvideo($id, Request $request)
	{
	    $title = $request->input('title');
        $link = $request->input('link_youtube');
        $today = date("Y-m-d H:m:s");
        $whereUpdate = array(
                              'title'=>$title,
                              'link_youtube'=>$link,
                              'creater'=>'admin',
                              'updated_at'=> $today);
        
        DB::table('videos')->where('id', $id)->update($whereUpdate);
        $list = DB::table('videos')->get();
        return json_encode($list);
	}
    
    
    
    /* ----------------------- End link Video  ------------------------------- */
    
    /* ----------------------- Image ------------------------------------------- */
    public function getimage()
	{
        
        $list = DB::table('images')->join('category','category.id','=','images.id_type')->select('images.*','category.name as nameType')->paginate(5);
        return response()->json($list);
	}
    
    
    public static function delimage($id)
    {
        DB::table('images')->where(array('id' => $id))->delete();
        $list = DB::table('images')->join('category','category.id','=','images.id_type')->select('images.*','category.name as nameType')->paginate(5);
        return response()->json($list);
    }
    
    public static function editimage(Request $request,$id)
    {
        $whereUpdate = array(
                              'title'=>$request->input('title'),
                              'id_type'=>$request->input('id_type') );
        DB::table('images')->where('id', '=' ,$id)->update($whereUpdate);
        $list = DB::table('images')->join('category','category.id','=','images.id_type')->select('images.*','category.name as nameType')->paginate(5);
        return response()->json($list);
    }
    
    
    
    /* ----------------------- End Image --------------------------------------- */


/* ----------------------- Employee Customer ------------------------------- */
    public function getEmployee()
    {
        $data = DB::table('employee')->paginate(16);
        return response()->json($data);
    }
    
    public function getAllEmployee(){
        $data = DB::table('employee')->get();
        return response()->json($data);
    }

    public function searchEmployee(Request $request)
    {
        $keyword = $request->q;
        $offset = $request->offset;
        $limit = $request->limit;
        $query = DB::table('employee')->where('lastname','like',"%{$keyword}%")
                                    ->orWhere('firstname','like',"%{$keyword}%")
                                    ->limit($limit)
                                    ->get();
        return response()->json($query);
    }

    public function createEmployee(Request $request)
    {
        // $numerical = $request->input('numerical');
        // dd($request->input('birthday'));
        $numerical = 0;
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $idperson = $request->input('idperson');
        $phone = $request->input('phone');
        $birthday = Carbon::createFromFormat('d/m/Y', $request->input('birthday'));
        $address = $request->input('address');
        $salary = $request->input('salary');
        // $image = $request->input('image');
         $password = md5('123456');
         $isCheckPhone = DB::table('employee')->where('phone', $phone)->first();

        $whereInsert = array(
                'numerical'     => $numerical,
                'firstname'     => $firstname,
                'lastname'      => $lastname,
                'idperson'      => $idperson,
                'phone'         => $phone,
                'birthday'      => $birthday,
                'address'       => $address,
                'salary'        => $salary,
                'password'      => $password,
                'role'          => 0,
                'status'        => 0
            );
        if ($isCheckPhone) {
            return response()->json('phone is already exists',400);            
        } else {
            DB::table('employee')->insert($whereInsert);
            return response()->json('Done');
        }
    }

    public function editEmployee($id, Request $request)
    {
        // $numerical = $request->input('numerical');
        $numerical = 0;
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $idperson = $request->input('idperson');
        $phone = $request->input('phone');
        $birthday = Carbon::createFromFormat('d/m/Y', $request->input('birthday'));
        $address = $request->input('address');
        $salary = $request->input('salary');
        $image = $request->input('image');
        $whereInsert = array(
                'numerical'     => $numerical,
                'firstname'     => $firstname,
                'lastname'      => $lastname,
                'idperson'      => $idperson,
                'phone'         => $phone,
                'birthday'      => $birthday,
                'address'       => $address,
                'salary'        => $salary,
                'status'        => 0
            );
        DB::table('employee')->where('id',$id)->update($whereInsert);
        return response()->json('Done');
    }

    public function destroyEmployee($id, Request $request)
    {
        $data = array(
                'status' => $request->input('status'),
            );
        DB::table('employee')->where('id',$id)->update($data);
        return response()->json('Done');
    }
    
    public function checkNumberPhone(Request $request)
    {
        $data = DB::table('employee')->where('phone', $request->input('phone'))->first();

         $whereIs = array(
                    'isCheck' => false
                );
        if ($data) {
            $whereIs = array(
                    'isCheck' => true
                );
        } else {
             $whereIs = array(
                    'isCheck' => false
                );
        }
        return response()->json($whereIs);
    }
    /* ----------------------- End Employee ------------------------------- */
    
    
    /* ----------------------- Point ------------------------------- */
    public function getPoint()
    {
        $data = DB::table('points')->where(array('status'=> '1'))->first();
        return response()->json($data);
    }
    
    public function createPoint($id,Request $request)
    {
        $point = $request->input('point');
        $money = $request->input('money');
        
        $today = date("Y-m-d H:m:s");
        
        $whereInsert = array(
                'point'     => $point,
                'money'     => $money,
                'created_at'      => $today,
                'status'        => '1'
            );
        DB::table('points')->where(array('id'=> $id))->update(array('status' => '0'));
        DB::table('points')->insert($whereInsert);
        $data = DB::table('points')->where(array('status'=> '1'))->first();
        return response()->json($data);
    }
    
    /* ----------------------- End Point ------------------------------- */
    
    /* ----------------------- Contact ------------------------------- */
    public function getContact()
    {
        $data = DB::table('contact')->orderBy('status','ASC')->paginate(10);
        return response()->json($data);
    }
    
    public function editContact(Request $request)
    {
        DB::table('contact')->where(array('id'=> $request->input('id')))->update(array('status' => '1'));
        $data = DB::table('contact')->orderBy('status','DESC')->paginate(10);
        return response()->json($data);
    }
    
    public function delContact(Request $request)
    {
        DB::table('contact')->where(array('id'=> $request->input('id')))->delete();
        $data = DB::table('contact')->orderBy('status','DESC')->paginate(10);
        return response()->json($data);
    }
    
    public function searchContact(Request $request)
    {
        if($request->input('keyword') == ""){
            $data = DB::table('contact')->orderBy('status','DESC')->paginate(10);
        }else{
            $keyword = $request->input('keyword');
            $data = DB::table('contact')->where('name','like',"%{$keyword}%")->orderBy('status','DESC')->paginate(10);
        }
        
        return response()->json($data);
    }
    
    
    /* ----------------------- End Contact ------------------------------- */

    /**
     * Search customer
     */
    public function searchCustomer(Request $request)
    {
        $keyword = $request->q;
        $offset = $request->offset;
        $limit = $request->limit;
        $data = DB::table('customer')->where('name','like',"%{$keyword}%")
                                    ->orWhere('phone','like',"%{$keyword}%")
                                    ->limit($limit)
                                    ->get();
        return response()->json($data);
    }

    /**
     * Get Category and sub catogory.
     */
    public function getServiceForBill()
    {
        $category = DB::table('category')->get();
        foreach ($category as $key) {
            $key->sub = DB::table('sub')->where('category', $key->id)
                                         ->get();
        }
        return response()->json($category);
    }

    public function searchService(Request $request)
    {
        $q = $request->q;
        $offset = $request->offset;
        $limit = $request->limit;
        $data = DB::table('sub')->where('name','like', "%{$q}%")
                                ->orWhere('id','like', "%{$q}%")
                                ->orWhere('price','like', "%{$q}%")
                                ->get();
        return response()->json($data);
    }

    public function getBill()
    {
        //get order status = 1
        $data = DB::table('bills')->where('status', 1)->get();

        foreach ($data as $key) {
            //get customer in order
            $key->customers = DB::table('customer')->where('id',$key->customer_id)->get();

            //get id service in table orders.
            $arrayService = DB::table('orders')->where('bill_id',$key->id)->get();

            //get info serivce in bill by array service id.
            $detail = array();
            foreach ($arrayService as $field) {
                
                $id_order = DB::table('orders')->select('id')
                                                ->where('bill_id', $key->id)
                                                ->where('service_id', $field->service_id)->first();
                $arrayEmployee = DB::table('employee_order')->select('id','firstname','lastname')
                                                            ->where('order_id', $id_order->id)
                                                            ->join('employee', 'employee_order.employee_id', '=', 'employee.id')
                                                            ->get(); 
                $service =  DB::table('sub')->where('id',$field->service_id)->first();
                $arrayOut = array(
                    'id'        => $service->id,
                    'name'      => $service->name,
                    'price'     => $service->price,
                    'employees' => $arrayEmployee,
                );
                $detail[] = $arrayOut;
            }
            $key->details = $detail;
        }
        return response()->json($data);
    }
    
    public function createBill(Request $request)
    {   
        Bills::store($request);
        return response()->json('Done',201);
    }

    public function deleteBill($id)
    {
        Bills::destroy($id);
        return response()->json('Done', 200);
    }

    public function updateBill($id, Request $request)
    {
        if ($request['status'] == 2) {
            if(count($request['customer']) < 1) {
                return response()->json('ChÆ°a cÃ³ khÃ¡ch hÃ ng',400);
            }
            if (count($request['orders'])  < 1) {
                return response()->json('ChÆ°a cÃ³ dá»‹ch vá»¥',400);
            }
            for ($i=0; $i < count($request['orders']); $i++) { 
                if (count($request['orders'][$i]['employees']) < 1) {
                    return response()->json('ChÆ°a cÃ³ nhÃ¢n viÃªn cho dá»‹ch vá»¥',400);
                }
            }
        }
        Bills::updateBill($id, $request);
        return response()->json('Done',200);   
    }
    
    public function getRank()		
    {		
        $data = DB::table('rank')->get();
         return response()->json($data);
    }
    /*--------------------------------- Revenue ------------------------------------------*/
    
    public function revenueInfo(Request $request)
    {
        $list = DB::table('orders')
                            ->join('bills','orders.bill_id','=','bills.id')
                            ->join('sub','orders.service_id','=','sub.id')
                            ->where('bills.updated_at','>=',$request->input('start'))
                            ->groupBy('service_id')
                            ->select('*',DB::raw('count(sub.id) as countService'),DB::raw('SUM(price) as TotalDeposits'))
                            ->get();
        return response()->json($list);
    }
    
    public function getInfoDate(Request $request)
    {
        $list = DB::table('orders')
                            ->join('bills','orders.bill_id','=','bills.id')
                            ->join('sub','orders.service_id','=','sub.id')
                            ->where('bills.updated_at','>=',$request->input('start'))
                            ->where('bills.updated_at','<=',$request->input('end'))
                            ->groupBy('service_id')
                            ->select('*',DB::raw('SUM(price) as TotalDeposits'))
                            ->get();
        return response()->json($list);
    }
    
    public function getAllEmployeeSalari(Request $request){
        $data = DB::table('employee')->where('role','=','0')->get();
        return response()->json($data);
    }
    
    public function countCustomer(Request $request){
        $list = DB::table('bills')
                            ->join('orders','orders.bill_id','=','bills.id')
                            ->join('sub','sub.id','=','orders.service_id')
                            ->join('customer','customer.id','=','bills.customer_id')
                            ->where('bills.updated_at','>=',$request->input('start'))
                            ->where('bills.updated_at','<=',$request->input('end'))
                            ->groupBy('customer.id')
                            ->select('*',DB::raw('count(customer.id) as TotalDeposits'),DB::raw('sum(sub.price) as Deposits'))
                            ->get();
        return response()->json($list);
    }
    public function countService(Request $request){
        $list = DB::table('orders')
                            ->join('sub','sub.id','=','orders.service_id')
                            ->join('bills','bills.id','=','orders.bill_id')
                            ->where('bills.updated_at','>=',$request->input('start'))
                            ->where('bills.updated_at','<=',$request->input('end'))
                            ->groupBy('orders.service_id')
                            ->select('*','sub.name as nameSub',DB::raw('count(orders.service_id) as TotalDeposits'))
                            ->get();
        return response()->json($list);
    }
    
    public function incomeEmployee(Request $request){
        $list = DB::Select('SELECT
                        e.employee_id ,
                        o.service_id ,
                        IFNULL((SELECT income FROM income WHERE income.idservice=o.service_id and income.idemployee = e.employee_id),0) as  income,
                        IFNULL((SELECT percent FROM income WHERE income.idservice=o.service_id and income.idemployee = e.employee_id),0) as  percent,
                        sum(IFNULL((SELECT income FROM income WHERE income.idservice=o.service_id and income.idemployee = e.employee_id),0)) as totalIncome,
                        sum(IFNULL((SELECT percent FROM income WHERE income.idservice=o.service_id and income.idemployee = e.employee_id),0)) as totalpercent
                        FROM `employee_order` as e,orders as o,income as i WHERE e.order_id = o.id and i.idemployee = e.employee_id and e.employee_id = '.$request->input("id"));
        $array = array($list);
        $result = array(); 
        $result = array_merge($result, array_flatten($array)); 
        return json_encode($result);
    }
    
    public function revenuesalaries(Request $request){
        $arrayEmployee = DB::table('employee')->get();
        
        $array1 = array();
        foreach ($arrayEmployee as $field) {
            $list = DB::Select('SELECT s.*, s.id as idService,
                            IFNULL((SELECT income FROM income WHERE income.idservice=s.id and income.idemployee = e.id),0) as income,
                            IFNULL((SELECT percent FROM income WHERE income.idservice=s.id and income.idemployee = e.id),0) as percent,
                            e.id,
                            (SELECT category.name FROM category WHERE category.id = s.category) as name1
                            FROM `sub` as s,`employee` as e WHERE e.id = '.$field->id);
            $income = 0;
            $percent = 0;
            foreach ($list as $field1) {
                $income += $field1->income;
                $percent += ($field1->percent * $field1->price)/100;
                
            }
            $array1 = array(
                            'employee' => $field,
                            'service' => $list,
                            'income' => $income,
                            'percent' => $percent
                );
            $detail[] = $array1;
        }
        return json_encode($detail);
    }
    public function serviceuse(Request $request){
        
        $arrayEmployee = DB::table('orders')
                                    ->join('bills','orders.bill_id','=','bills.id')
                                    ->join('employee_order','employee_order.order_id','=','orders.id')
                                    ->whereDate('updated_at', 'LIKE', $request->input('date').'%')->get();
        
        foreach ($arrayEmployee as $field1) {
            $arrayincome = DB::table('income')
                                        ->join('sub','sub.id','=','income.idservice')
                                        ->where('idemployee','=',$field1->employee_id)
                                        ->where('idservice','=',$field1->service_id)
                                        ->select('*','income.income as incomemoney')
                                        ->get();
            foreach ($arrayincome as $field2) {
                $field2->incomemoney = $field2->incomemoney;
                $field2->percent = ($field2->percent * $field2->price)/100;
            }                          
            $field1->income = $arrayincome;
        }
        $employee = DB::table('employee')->get();
        foreach ($employee as $field3) {
            $field3->income = 0;
            $field3->percent = 0;
            for ($i = 0; $i < count($arrayEmployee); $i++) {
                if($arrayEmployee[$i]->income != null && $field3->id == $arrayEmployee[$i]->employee_id){
                    $field3->income += $arrayEmployee[$i]->income[0]->incomemoney;
                    $field3->percent += $arrayEmployee[$i]->income[0]->percent;
                }
                 
            }
            
        }
        
        return json_encode($employee);
    }
    
    
    
    
    
    /*--------------------------------- End Revenue ------------------------------------------*/
    
   
    
    
    
}





?>