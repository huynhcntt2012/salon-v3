<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Database\themes;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\JsonableInterface;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\LoginController;



class BlogController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        $nameThemes = themes::getThemesPresent();
        
        if($request->input('page') == ''){
            $current = 0;
        }else{
            $current = $request->input('page');
        }
        
        $array = array('themes'=> $nameThemes,'url' =>'blog');
        
        $total = DB::table('posts')->where(array('post_status' => 'blog'))->get();
        
        $post = DB::table('posts')->where(array('post_status' => 'blog'))->offset($current*5)->limit(5)->get();
        
        return view('themes/'.themes::getThemesPresent().'/user/blog')
                                                ->with('arrayBase',$array)
                                                ->with('post',$post)
                                                ->with('totalpage',count($total))
                                                ->with('currentpage',$current);
	}
    
    public function detail($id)
	{
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'detail/index');
        
        $post = DB::table('posts')->where(array('ID' => $id))->get();
        
        return view('themes/'.themes::getThemesPresent().'/user/detail/index')
                                                ->with('arrayBase',$array)
                                                ->with('data',$post);
	}
        
    public function blogHome($keyword=null,$status=null){
    $title = "Bảng Blog Chính";

    $selecteditem = 2;
    $selectedmenu = 3;
    
    if(SessionController::checkAdmin('keyAdmin') == false){
        return redirect()->to('/admin');
    }
    $keyword=Input::get('keyword');
    $status =Input::get('status');
    $posts= DB::table('posts')->where("post_title","LIKE","%".$keyword."%");
                              
    if($status != null){
        $posts = $posts->where("post_status",$status);
    }  
    if($status != null && $keyword != null){
        $posts= $posts->where("post_title","LIKE","%".$keyword."%")->where("post_status",$status);
    }
    $posts= $posts->orderBy('ID','DESC')->paginate(10);
    return view('admin.pages.blogHome',['posts' => $posts])->with('title',$title)
                                ->with('selecteditem',$selecteditem)
                                ->with('selectedmenu',$selectedmenu)
                                ->with('keyword',$keyword)
                                ->with('status',$status)
                                ->with('lastPage',$posts->lastPage()) 
                                ->with('currentPage',$posts->currentPage());
	}
    public function createBlog($bien = null){
        
        $title = "Bảng Blog Chính";
        if(SessionController::checkAdmin('keyAdmin') == false){
                        return redirect()->to('/admin');
        }
        $selecteditem = 2;
        $selectedmenu = 3;
        $data = DB::table('posts')->where(array('ID' => $bien))->get();
        if($bien==null){
            return view('admin.pages.createBlog')->with('title',$title)
                                ->with('selecteditem',$selecteditem)
                                ->with('selectedmenu',$selectedmenu)
                                ->with('post',$data);   
        }  else {
            
            if(count($data)>0){
                return view('admin.pages.createBlog')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('post',$data);
            }else{
                return view('admin.pages.error')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);                         
                }
            }
        
    }
        public function getImages(){
            $data = DB::table('images')->select()->get();
            return json_encode($data);
        }
        public function deleteblog($bien){
            DB::table('posts')->where('ID',$bien)->delete();
            return back();
        }
        public function createBlogPost(Request $request){
            
            $title = "Bảng Tin Chính";

            $selecteditem = 2;
            $selectedmenu = 3;
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            
            $id = $request->input('post_id');
            $today = date("Y-m-d H:m:s");
            $title_post = $request->input('title');
            $content = $request->input('content');
            $img = $request->input('name-img');
            $post_status = $request->input('getHome');
            
            $status=true;
            switch($post_status){
                case '1':
                    $post_status = "homeb";
                    break;
                case '2':
                    $post_status = "homes";
                    break;
                case '3':
                    $post_status = "blog";
                    break;
                case '4':
                    $post_status = "service";
                    break;
                default:
                    $post_status = "blog";
                    break;
            }
            try{
                if($id!=""){
                DB::table('posts')
                ->where('ID',$id)
                ->update(['created_at' => $today,
                    'post_content' =>$content,
                    'post_title' => $title_post,
                    'post_image' => $img,
                    'post_status' => $post_status]);
                }  else {
                    DB::table('posts')->insert([
                        ['post_date' => $today,
                        'post_content' =>$content,
                        'post_title' => $title_post,
                        'post_image' => $img,
                        'post_status' => $post_status],
                    ]);
                }
                $status = "Thao tác thành công";
            }  catch (Exception $e){
                $status = "Thao tác thất bại";
            }           
            $data = array(['post_id'=>$id,
                    'post_date' => $today,
                    'post_content' =>$content,
                    'post_title' => $title_post,
                    'post_image' => $img,
                    'post_status' => $post_status,
                    'status' => $status]
                    );
            $text ="abc";
            return back()->with("post",$data)->with('status',$text);
                                    
                                   
        }
        public function imageUploadPost(Request $request)
        {
            /**
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
           
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $imageName);
             *  **/
             $imageName ="";
             $status =1;
            if($request->hasFile('file')){
                $imageName = time().'.'.$request->file('file')->getClientOriginalExtension();
                $request->file('file')->move(public_path('images'), $imageName);
                
                $status = 0;          
            }  else {
                $status =1;
            }
             $array =array('status'=>$status,'imageName'=>$imageName);
             return json_encode($array);
           
        }
        
}