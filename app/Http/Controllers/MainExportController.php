<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;


class MainExportController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    
    public function revenue()
	{
        $title = "Doanh Thu";

        $selecteditem = 1;
        $selectedmenu = 9;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'revenue');
        
        return view('admin/pages/statistic/index')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function salaries()
	{
        $title = "Luong Nhân Viên";

        $selecteditem = 2;
        $selectedmenu = 9;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'salaries');
        
        return view('admin/pages/statistic/salaries')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function revenexpen()
	{
        $title = "Các Khoản Chi Thu";

        $selecteditem = 3;
        $selectedmenu = 9;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'revenexpen');
        
        return view('admin/pages/statistic/revenexpen')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function customersta()
	{
        $title = "Thống Kê Khách Hàng";

        $selecteditem = 4;
        $selectedmenu = 9;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'customersta');
        
        return view('admin/pages/statistic/customersta')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function servicessta()
	{
        $title = "Thống Kê Dịch Vụ";

        $selecteditem = 5;
        $selectedmenu = 9;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'servicessta');
        
        return view('admin/pages/statistic/servicessta')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    
    
}