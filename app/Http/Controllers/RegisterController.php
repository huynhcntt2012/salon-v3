<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;
use App\Http\Database\themes;

class RegisterController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'register','message' => '');

        return view('themes/'.$nameThemes.'/user/register')->with('arrayBase',$array);
	}
    
    public function change(Request $request){
        
        $nameThemes = themes::getThemesPresent();

        $user = DB::table('customer')->where(array('phone' => Session::get('phone')))->first();
        
        $whereUpdate = array( 'password'=>md5($request->input('pass')));
        
        DB::table('customer')->where('phone', Session::get('phone'))->update($whereUpdate);
        
        return Redirect::to('userinfor');
        
    }
    
    
    public function process(Request $request)
	{
        $nameThemes = themes::getThemesPresent();
        
        $arraywhere = array(
                            'phone' => $request->input('phone')
                            );
        $count = DB::table('customer')->where($arraywhere)->get();
        
        if(count($count) == 0){
            $today = date("Y-m-d H:m:s");
            Session::put('userinfor','true');
            Session::put('phone',$request->input('phone'));
            $arrayinsert = array(
                                'name' => $request->input('user_name'),
                                'phone' => $request->input('phone'),
                                'birthday' => $request->input('birthday'),
                                'address' => $request->input('address'),
                                'password' => md5($request->input('pass1')),
                                'created_at' => $today);
            DB::table('customer')->insert($arrayinsert);
        }
        //Session::put('userinfor','true');
        if(SessionController::checkAdmin('userinfor') == false){
            
            $array = array('themes'=> $nameThemes,'url' =>'register','message' => 'Đăng Ký Thất Bại');
            return view('themes/'.$nameThemes.'/user/login')->with('arrayBase',$array);
        }
        return Redirect::to('userinfor');
        
	}
    
    public function user()
	{
        $nameThemes = themes::getThemesPresent();
        
        if(SessionController::checkAdmin('userinfor') == false){
            return Redirect::to('home');
            
        }
        //$user = DB::table('customer')->where(array('phone' => Session::get('phone')))->first();
        $user = DB::Select('SELECT customer.*,rank.name as nameRank FROM rank,customer WHERE rank.point > (customer.point_gift+customer.point_use) AND customer.phone = '.Session::get('phone').' GROUP BY customer.id');
        $array = array('themes'=> $nameThemes,'url' =>'userinfor','message' => '','session' => Session::get('phone'));
        return view('themes/'.$nameThemes.'/user/userinfor')->with('arrayBase',$array)
                                                           ->with('users',$user[0]);
	}
    
    
    
}