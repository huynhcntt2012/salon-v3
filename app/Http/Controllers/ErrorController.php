<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\Models\ArrayBase;
use App\Http\Database\themes;



class ErrorController extends Controller {
    
    public function __construct()
	{
		$this->middleware('guest');
	}
    
    public function index()
	{
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'home','data' =>'data');
        
        return view('themes/'.themes::getThemesPresent().'/user/500')->with('arrayBase',$array);
    }
}


?>