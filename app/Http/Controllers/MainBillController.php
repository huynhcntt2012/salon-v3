<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;

class MainBillController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    
    public function bill()
	{
        $title = "Phiếu Dịch Vụ";
        $selecteditem = 1;
        $selectedmenu = 1;
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'bill');
        
        return view('admin/pages/bill/index')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
}