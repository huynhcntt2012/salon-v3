<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;
use Illuminate\Support\Facades\Input;


class MainNewController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    
    public function newhome()
	{
        $title = "Bảng Tin Chính";

        $selecteditem = 1;
        $selectedmenu = 3;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        $data = DB::table('posts')->orderBy('ID', 'DESC')->get();
        
        $array = array('url' =>'newhome','data' => $data);
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function edithome(Request $request)
	{
        $title = "Bảng Tin Chính";

        $selecteditem = 1;
        $selectedmenu = 3;
        
        $id = $request->input('id');
        
        $id_del = Input::get('id_del');
        
        $data = DB::table('posts')->where(array('ID' => $id))->get();
        
        if(SessionController::checkAdmin('keyAdmin') == false || $id == "" || count($data) == 0){
            return Redirect::to('admin/newhome');
        }
        
        if($id_del != ""){
            
            $arraydel = array(
                                    'ID' => $request->id_del
                                    );
            DB::table('posts')->where($arraydel)->delete();
            $data = DB::table('posts')->get();
            $array = array('url' =>'newhome','data' => $data);
            return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
            
        }else{
            $array = array('url' =>'edithome','data' => $data);
        }
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function createnewhome()
	{
        $title = "Bảng Tin Chính";

        $selecteditem = 1;
        $selectedmenu = 3;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        $array = array('url' =>'createnewhome'); 
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function newblog()
	{
        $title = "Bảng Tin Blog";

        $selecteditem = 2;
        $selectedmenu = 3;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'newblog');
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function newservice()
	{
        $title = "Bảng Tin Dịch Vụ";

        $selecteditem = 3;
        $selectedmenu = 3;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'newservice');
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function post(Request $request)
	{
        $title = "Bảng Tin Dịch Vụ";

        $selecteditem = 1;
        $selectedmenu = 3;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $status = $request->input('type_action');
        $id = $request->input('id');
        $today = date("Y-m-d H:m:s");
        if($status == "false"){
            if($request->file('img')){
                $img = $request->file('img')->getClientOriginalName();
                move_uploaded_file($_FILES["img"]["tmp_name"], 'resources/views/themes/salon/public/img/'.$_FILES["img"]["name"]);
            }else{
                $img = "";
            }
            $arrayinsert = array(
                                    'post_title' => $request->input('title'),
                                    'post_image' => $img,
                                    'post_content' => $request->input('text-content'),
                                    'post_date' => $today,
                                    'post_status' => 'home');
            DB::table('posts')->insert($arrayinsert);
            
            
        }else{
            if($request->file('img')){
                $img = $request->file('img')->getClientOriginalName();
                move_uploaded_file($_FILES["img"]["tmp_name"], 'resources/views/themes/salon/public/img/'.$_FILES["img"]["name"]);
                $arrayupdate = array(
                                    'post_title' => $request->input('title'),
                                    'post_image' => $img,
                                    'post_content' => $request->input('text-content'),
                                    'post_date' => $today,
                                    'post_status' => 'home');
            }else{
                $arrayupdate = array(
                                    'post_title' => $request->input('title'),
                                    'post_content' => $request->input('text-content'),
                                    'updated_at' => $today,
                                    'post_status' => 'home');
            }
            
            DB::table('posts')->where(array('ID'=>$id))->update($arrayupdate);
                                        
        }
        $data = DB::table('posts')->orderBy('ID', 'desc')->get();
        $array = array('url' =>'newhome','data' => $data);
        return Redirect::to('admin/newhome');
        
	}
    
    
    
}