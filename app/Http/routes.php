<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//chrome.exe --user-data-dir="C:/Chrome dev session" --disable-web-security



/* ------------------------------ Route User ------------------------------ */

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index');

Route::get('/about', 'AboutusController@index');

Route::get('/services', 'ServicesController@index');

Route::get('/blog', 'BlogController@index');

Route::get('/contact', 'ContactController@index');

Route::post('/contact', 'ContactController@contact');

Route::get('/gallery', 'GalleryController@index');

Route::get('/gallery/image', 'GalleryController@image');

Route::get('/gallery/video', 'GalleryController@video');

Route::get('/login', 'LoginController@index');

Route::post('/login', 'LoginController@loginUser');

Route::get('/user', 'UserController@index');

Route::get('/register', 'RegisterController@index');

Route::post('/register', 'RegisterController@process');

Route::get('/userinfor', 'RegisterController@user');

Route::get('/logout', 'LoginController@logoutUser');

Route::get('/detail/{id}','BlogController@detail');

Route::post('/changepass','RegisterController@change');

Route::get('/changepass','ErrorController@index');

/* ------------------------------ End Route USer ------------------------------ */


/* ------------------------------ Route Admin ------------------------------ */


Route::get('/admin', 'LoginController@admin');

Route::post('/admin', 'LoginController@login');

Route::get('/admin/main', 'MainController@index');

Route::get('/admin/device', 'MainDeviceContronller@index');

/* ---------------------------------- Main Page -------------------------------------------*/

    Route::get('/admin/logout', 'LoginController@logout');
    
    Route::get('/admin/bill', 'MainBillController@bill');
    
    Route::get('/admin/customerinfo', 'MainCustomerController@customerinfo');
    
    Route::get('/admin/customerdate', 'MainCustomerController@customerdate');
    
    Route::get('/admin/customer', 'MainCustomerController@customer');
    
    Route::post('/admin/customer', 'MainCustomerController@customer');
    
    Route::get('/admin/newhome', 'MainNewController@newhome');
    
    Route::get('/admin/createnew', 'MainNewController@createnewhome');
    
    Route::get('/admin/edithome', 'MainNewController@edithome');
    
    Route::post('/admin/posts', 'MainNewController@post');
    
    Route::get('/admin/newblog', 'MainNewController@newblog');
    
    Route::get('/admin/newservice', 'MainNewController@newservice');
    
    Route::get('/admin/employeetour', 'MainEmployeeController@employeetour');
    
    Route::get('/admin/employee', 'MainEmployeeController@employee');
    
    Route::get('/admin/servicecategory', 'MainServicesController@servicecategory');
    
    Route::get('/admin/servicesub', 'MainServicesController@servicesub');
    
    Route::get('/admin/image/{keyword?}', 'MainImageController@image');
    
    Route::post('/admin/image/{keyword?}', 'MainImageController@create');

    Route::get('/admin/image/{id}', 'MainImageController@edit');

    Route::get('/admin/image/destroy/{id}', 'MainImageController@destroy');

    Route::get('/admin/uploadimage', 'MainImageController@upload');
    
    Route::get('/admin/video/{keyword?}', 'MainVideoController@video');

    Route::get('/admin/video/{id}', 'MainVideoController@edit');

    Route::get('/admin/video/destroy/{id}', 'MainVideoController@destroy');

    Route::get('/admin/uploadvideo', 'MainVideoController@upload');
    
    Route::get('/admin/point', 'MainCareController@point');
    
    Route::get('/admin/contact', 'MainCareController@contact');
    
    Route::get('/admin/revenue', 'MainExportController@revenue');
    
    Route::get('/admin/salaries', 'MainExportController@salaries');
    
    Route::get('/admin/revenexpen', 'MainExportController@revenexpen');
    
    Route::get('/admin/customersta', 'MainExportController@customersta');
    
    Route::get('/admin/servicessta', 'MainExportController@servicessta');

/* ---------------------------------- End Main Page -------------------------------------------*/


/* ------------------------------ End Route Admin ------------------------------ */

    Route::get('/admin/homeBlog/{keyword?}/{status?}','BlogController@blogHome');
    
    Route::get('/admin/createblog/{bien?}','BlogController@createBlog');
    
    Route::get('/admin/deleteblog/{bien?}','BlogController@deleteblog');
    
    Route::post('image-upload','BlogController@imageUploadPost');
    
    Route::get('getImages','BlogController@getImages');
    
    Route::post('createBlogPost','BlogController@createBlogPost');
    
    Route::post('searchBlogPost','BlogController@searchBlogPost');
    
/* ------------------------------------- Api ----------------------------------------- */
/* ----- Category service -------- */
Route::get('admin/api/getCategory','ApiController@getCategory');

Route::get('admin/api/category/{id}','ApiController@getDetailCategory');

Route::post('admin/api/createCategory','ApiController@createCategory');

Route::put('admin/api/editCategory/{id}','ApiController@editCategory');

Route::delete('admin/api/delCategory/{id}','ApiController@delCategory');


/* ------------End Del --------- */


/*  -------------------- getSettingService ------------------------- */

Route::get('admin/api/getSettingEmployee/{id}','ApiController@getSettingEmployee');

Route::get('admin/api/getSettingService/{id}','ApiController@getSettingService');

Route::post('admin/api/setServiceEmployee/{idemployee}/{idservice}/{income}/{percent}','ApiController@setServiceEmployee');

/*  -------------------- End getSettingService ------------------------- */

/* ----- Sub -------- */
/* ------------ Load --------- */
Route::get('admin/api/getSub','ApiController@getSub');
Route::get('admin/api/getSubAll','ApiController@getSubAll');

/**
 * Get sub by id category
 */
Route::get('admin/api/category/{id}/sub', 'ApiController@getSingleSub');

Route::post('admin/api/createSub','ApiController@createSub');

Route::post('admin/api/editSub','ApiController@editSub');

Route::put('admin/api/editSub/{id}','ApiController@editSub');

Route::delete('admin/api/delSub/{id}','ApiController@delSub');

/**
 * Get category and child of category.
 */
Route::get('admin/api/service','ApiController@getServiceForBill');

Route::get('admin/api/service/search','ApiController@searchService');
/* ------------End service --------- */

/* ----- Customer -------- */
Route::get('admin/api/getCustomer','ApiController@getCustomer');

Route::get('admin/api/getSearch','ApiController@getSearchHigh');

Route::get('admin/api/getSearchPhone','ApiController@getSearchPhone');

Route::get('admin/api/getSearchDate','ApiController@getSearchDate');

/**
 * search customer in bill
 */
Route::get('admin/api/customer/search', 'ApiController@searchCustomer');
/* ----- End Search Customer -------- */



/* --------------------------  link video  ------------------------------*/

Route::post('admin/api/createvideo','ApiController@create');

Route::get('admin/api/getvideo','ApiController@getvideo');

Route::put('admin/api/editvideo/{id}','ApiController@editvideo');

Route::delete('admin/api/delvideo/{id}','ApiController@delvideo');
/* -------------------------- End link video  ------------------------------*/


/* --------------------------  Image  ------------------------------*/



Route::get('admin/api/getimage','ApiController@getimage');

Route::put('admin/api/editimage/{id}','ApiController@editimage');

Route::delete('admin/api/delimage/{id}','ApiController@delimage');
/* -------------------------- End Image  ------------------------------*/



/* ----- Employee -------- */

Route::post('admin/api/employee', 'ApiController@checkNumberPhone');

Route::get('admin/api/employees', 'ApiController@getEmployee');

Route::get('admin/api/employee/search','ApiController@searchEmployee');

Route::post('admin/api/employee/create', 'ApiController@createEmployee');

Route::post('admin/api/employee/{id}/edit', 'ApiController@editEmployee');

Route::post('admin/api/employee/{id}', 'ApiController@destroyEmployee');

Route::get('admin/api/employeesAll', 'ApiController@getAllEmployee');




/* ----- Point -------- */
Route::get('admin/api/getpoint', 'ApiController@getPoint');

Route::post('admin/api/createpoint/{id}', 'ApiController@createPoint');


/* ------------------------------------- End Point ----------------------------------------- */


/* ----- Contact -------- */
Route::get('admin/api/getcontact', 'ApiController@getContact');



/* ------------------------------------- End Contact ----------------------------------------- */

/**
 * Buill
 */

Route::get('admin/api/bill', 'ApiController@getBill');

Route::post('admin/api/bill/create', 'ApiController@createBill');

/**
 * Delete Bill.
 */
Route::post('admin/api/bill/{id}/delete', 'ApiController@deleteBill');

Route::post('admin/api/bill/{id}/update', 'ApiController@updateBill');
/* ------------------------------------- End Api ----------------------------------------- */

/* ------------------------------------- Revenue ----------------------------------------- */

Route::post('admin/api/revenue/getInfo', 'ApiController@revenueInfo');

Route::post('admin/api/revenue/getInfoDate', 'ApiController@revenueDate');

Route::post('admin/api/revenue/employeeSalaries', 'ApiController@getAllEmployeeSalari');

Route::post('admin/api/revenue/customer', 'ApiController@countCustomer');

Route::post('admin/api/revenue/getservice', 'ApiController@countService');

Route::get('admin/api/revenue/income', 'ApiController@incomeEmployee');

/* ------------------------------------- End Revenue ----------------------------------------- */

Route::get('admin/api/rank', 'ApiController@getRank');

Route::post('admin/api/editContact', 'ApiController@editContact');

Route::post('admin/api/delContact', 'ApiController@delContact');

Route::get('admin/api/searchContact', 'ApiController@searchContact');

 
// /**
//  * Get point
//  */
// Route::get('admin/api/point', 'ApiController@getPoint');



Route::get('admin/api/revenuesalaries', 'ApiController@revenuesalaries');

Route::get('admin/api/serviceuse', 'ApiController@serviceuse');

include_once "routesPhone.php";
