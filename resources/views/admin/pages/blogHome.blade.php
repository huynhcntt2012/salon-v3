@extends('admin.main')
@section('css')
    <link href="{{asset('/public/asset/css/mainblog.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('js')
    <script src="{{asset('/public/asset/js/mainblog.js')}}" type="text/javascript"></script>
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
            $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        });
        
    </script>
@stop
@section('content')
<h2>Bài viết <a class="btn btn-success" href="createblog">Tạo tin</a></h2>
    
<form id="frm_search" action="{{url('admin/homeBlog')}}" method="Get" class="form-inline" style="padding-right:5px;margin-bottom: 10px;">
        <div class="form-group">
          <label for="email">Tiêu đề</label>
          <input type="text" class="form-control" name="keyword" value="{{$keyword}}"  id="keyword">
        </div>
        <div class="form-group">
          <label for="pwd">Vị trí</label>
          <select class="form-control" name="status"  id="status">
                <option <?php if($status == "") { echo 'selected="selected"'; }?> value=""></option>
                <option <?php if($status == "homeb") { echo 'selected="selected"'; }?> value="homeb">Trang chủ Blog</option>
                <option <?php if($status == "homes") { echo 'selected="selected"'; }?> value="homes">Trang Tin Dịch Vụ</option>
                <option <?php if($status == "blog") { echo 'selected="selected"'; }?> value="blog">Trang Blog</option>
                <option <?php if($status == "service") { echo 'selected="selected"'; }?> value="service">Trang Dịch Vụ</option>
          </select>
        </div>
    <button type="submit" id="submit" class="btn btn-default">Tìm kiếm</button>
    </form> 
    <table class="table table-blog" style="background-color: #fff ">
      <thead>
        <tr>
          <th>
              Stt         
          </th>
          <th>
              Tiêu đề            
          </th>
          <th>Người tạo</th>
          <th>Ngày tạo</th>
          <th>Vị trí</th>
        </tr>
      </thead>
      <tbody>
      <?php $stt = 1;?>
          @foreach ($posts as $posts)

            <tr>
                <td>{{$stt++}}</td>
            <td>{{$posts->post_title}}
                <div class="box-control-table">
                 <div class="control-table">
                     <a href="{{asset('admin/createblog/'.$posts->ID.'')}}">Cập Nhật</a> |
                    <a data-href="{{asset('admin/deleteblog/'.$posts->ID.'')}}" data-toggle="modal" data-target="#confirm-delete" >Xóa</a>
                </div>
              </div>
            </td>
            <td>{{$posts->post_name}}</td>
            <td>{{$posts->post_date}}</td>
            <td>{{$posts->post_status}}</td>
          </tr>       
        @endforeach

       
      </tbody>
    </table> 
    <div class="container">
        <div class="row" style="text-align: center;">
               <ul class="pagination">
                    <li><a href="{{url('admin/homeBlog?page='.($currentPage-1).'&keyword='.$keyword.'&status='.$status.'')}}">&laquo;</a></li>
                    @for($i=0;$i<$lastPage;$i++)
                    <li class="{{($i==($currentPage-1))?"active":""}}" ><a href="{{url('admin/homeBlog?page='.($i+1).'&keyword='.$keyword.'&status='.$status.'')}}">{{$i+1}}</a></li>
                    @endfor
                   <li><a href="{{url('admin/homeBlog?page='.($currentPage+1).'&keyword='.$keyword.'&status='.$status.'')}}">&raquo;</a></li>
                </ul>
              
        </div>
      <div class="row">
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Thông báo</h4>
                </div>
            
                <div class="modal-body">              
                    <p>Bạn muốn xóa bài viết này ?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <a class="btn btn-danger btn-ok">Đồng ý</a>
                </div>
            </div>
        </div>
    </div>
        
@stop


