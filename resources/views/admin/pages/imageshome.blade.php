@extends('admin.main')
@section('js')
	<script src="{{asset('/public/dist/js/image.js')}}"></script>
@endsection
@section('css')
    <link href="{{asset('/public/asset/css/mainblog.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('js')
    <script src="{{asset('/public/asset/js/mainblog.js')}}" type="text/javascript"></script>
@stop
@section('content')
<section id="imageload" class="content">
    <div class="modal fade" id="conform" tabindex="-1" role="dialog">
        <div class="modal-dialog">
        	<div class="modal-content">
        		<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        				<span aria-hidden="true">×</span>
        			</button>
                    <h4 class="modal-title">Bạn muốn xóa giai đoạn <strong id="nass" ng-model="add_title">@{{ add_title }}</strong></h4>
        		</div>
        		<div class="modal-footer">
                    <div id="revenue-footer" class="col-lg-12">
        				<button type="button" class="col-lg-5 btn btn-default pull-left" data-dismiss="modal">Thoát</button>
        				<button type="button" class="col-lg-5 btn btn-default pull-right"  @click="deleteimage(add_id)">Xóa</button>
                    </div>
        		</div>
        	</div>
        </div>
    </div>
    <div class="modal fade" id="add-category" tabindex="-1" role="dialog">
    	<div class="modal-dialog">
            <div class="modal-content">
            	<div class="modal-header">
            		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            			<span aria-hidden="true" data-dismiss="modal" data-dismiss="modal">×</span>
            		</button>
                    <h4 class="modal-title">Sửa thông tin hình ảnh</h4>
            	</div>
            	<div class="modal-body">
            		<div class="row">
            			<div class="col-sm-12">
            				<div class="form-horizontal">
                                <div class="form-group">
            						<label for="name-category" class="col-sm-2 control-label">Tên</label>
            						<div class="col-sm-8">
            							<input type="text" class="form-control" id="title"  name="title" placeholder="" v-model="add_title">
            						</div>
            	                </div>
                                <div class="form-group">
            						<label for="name-category" class="col-sm-2 control-label">Loại</label>
            						<div class="col-sm-8">
                                        
            							<select name="cate" id="cate" v-model="add_cate" class="form-control">
											<option v-for="cate in cate" value="@{{ cate.id }}"  >@{{ cate.name }}</option>
										</select>
            						</div>
            	                </div>
            	           	</div>
            			</div>
            		</div>
            	</div>
            	<div class="modal-footer">
                    <div id="revenue-footer" class="col-lg-12">
            			<button type="button" class="col-lg-5 btn btn-default pull-left" data-dismiss="modal">Thoát</button>
                        <button  type="button" class="col-lg-5 btn btn-default pull-right"  @click="editImage()">Sửa</button>
                    </div>
            	</div>
            </div>
        </div>
    </div>
    <h2>Kho Ảnh <a class="btn btn-success" href="uploadimage">Upload Ảnh</a></h2>
    <form id="frm_search" action="{{url('admin/image')}}" method="Get" class="form-inline" style="padding-right:5px;margin-bottom: 10px;">
            <div class="form-group">
              <label for="email">Tiêu đề</label>
              <input type="text" class="form-control" name="keyword" value="{{$keyword}}"  id="keyword">
            </div>
        <button type="submit" id="submit" class="btn btn-default">Tìm kiếm</button>
      </form>
            
        <table class="table table-blog" style="background-color: #fff ">
          <thead>
            <tr>
              <th>
                  Stt         
              </th>
              <th>
                  Tiêu đề            
              </th>
              <th>Hình ảnh</th>
              <th>Thể Loại</th>
              <th>Người tạo</th>
              <th>Tùy Chọn</th>
            </tr>
          </thead>
            <tbody>
                <tr v-for="image1 in image">
                    <td class="col-sm-1">@{{ $index + 1 }}</td>
                    <td class="col-sm-2">@{{ image1.title }}</td>
                    <td class="col-sm-4"><img style="max-width: 100px; max-height: 100px;" src="@{{ '../public/images/'+ image1.name}}"/></td>
                    <td class="col-sm-1">@{{ image1.nameType }}</td>
                    <td class="col-sm-2">@{{ image1.creater }}</td>
                    <td class="col-sm-2">
                        <a class="btn" data-toggle="modal" data-target="#add-category" @click="getimageinfo(image1.id,image1.title,image1.id_type)" title="Sửa" >
                            <i class="fa fa-edit" ></i>
                        </a>
                      
                        <a class="btn" @click="getimageinfo(image1.id,image1.title,image1.id_type)" data-toggle="modal" data-target="#conform" title="Xóa" >
                            <i class="fa fa-times-circle" ></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="container">
            <div class="row" style="text-align: center;">
                            <ul class="pagination ">
								<li @click="switchPage(1)"><a>&laquo;</a></li>
								<li v-for="page in image1.total_page"  @click="switchPage(page + 1)" :class="((page + 1) == image.current_page) ? 'active' : ''">
                                    <a>@{{ page + 1 }}</a></li>
								<li @click="switchPage(image.last_page)" ><a>&raquo;</a></li>
							</ul>
                          
                    </div>
        </section>
@stop


