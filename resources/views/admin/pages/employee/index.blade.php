@extends('admin.main')

@section('css')
	<style>
/* 		.abc {
			border: 0;
		} */
		.name-employee {
			text-transform: capitalize;
		}
	</style>
@endsection

@section('content')
	<section class="content" id="employee">
		<div class="row">
			<div class="col-md-12 list-unstyled">
				<div class="col-md-9  navbar-btn">
					<input class="form-control abc" type="text" v-on:keyup="getKeySearch()" placeholder="Nhập tên cần tìm..." v-model="keyword">
				</div>
				<div class="col-md-3">
					<button type="button" class="btn navbar-btn bg-navy btn-flat pull-right"
							data-toggle="modal" 
							data-target="#add-employee"
							@click="switchTemplate(0)"
							>
							Thêm nhân viên
					</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" v-for="employee in employee_list">
				<div class="box box-widget widget-user">
					<div :class="employee.status == 0 ? 'widget-user-header bg-aqua-active' : 'widget-user-header bg-red'">
						<h3 class="widget-user-username name-employee">@{{ employee.firstname }} @{{ employee.lastname }}</h3>
					</div>
					<div class="widget-user-image">
						<img class="img-circle" src="http://coverphoto.erth.biz/wp-content/uploads/coverphotos/Pathway-hiking-trail-nature-green-facebook-cover-photos-128x128.jpg" alt="User Avatar">
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-sm-6 border-right">
								<div class="description-block">
									<h5 class="description-header">CMND</h5>
									<span class="description-text">@{{ employee.idperson }}</span>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="description-block">
									<h5 class="description-header">Chức năng</h5>
									<div class="btn-group">
										<a class="btn-sm btn-info btn-flat pull-left dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<i class="fa fa-cogs"></i>
										</a>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a 
												data-toggle="modal" 
												data-target="#add-employee"
												@click="switchTemplate(1,employee.id)"
											>Chỉnh sửa</a></li>
											<li v-if="employee.status == 0" ><a @click="blockEmployee(employee.id,1)">Khóa tài khoản</a></li>
											<li v-else ><a @click="blockEmployee(employee.id,0)">Mở tài khoản</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" v-if="!is_search">
				 <ul class="pagination">
					<li><a @click="fetchDataEmployee(1,0)">&laquo;</a></li>
					<li v-for="p in page.total_page" :class="(p + 1) == page.current ? 'active' : ''"><a @click="fetchDataEmployee(p+1,0)">@{{ p + 1 }}</a></li>
					<li><a @click="fetchDataEmployee(page.last,0)">&raquo;</a></li>
                </ul>
			</div>
		</div>
		<div class="modal fade" id="add-employee" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Thêm nhân viên </h4>
						<span class="text-muted">Lưu ý: những thông tin có dấu (*) là những thông tin bắt buộc.</span>
					</div>

					<div class="modal-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-horizontal">
									<div class="form-group">
										<label for="firstname" class="col-sm-4 control-label">Họ (<label class="text-red">*</label>):</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="firstname" placeholder="" v-model="employee.firstname">
										</div>
					                </div>
					                <div class="form-group">
										<label for="lastname" class="col-sm-4 control-label">
												Tên (<label class="text-red">*</label>):
										</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="lastname" placeholder="" v-model="employee.lastname">
										</div>	
					                </div>
					                <div class="form-group">
										<label for="idperson" class="col-sm-4 control-label">Số chứng minh (<label class="text-red">*</label>):</label>
										<div class="col-sm-8">
											<input type="number" class="form-control" id="idperson" placeholder="" v-model="employee.idperson">
										</div>
					                </div>
					                <div class="form-group">
										<label for="phone" class="col-sm-4 control-label">Số điện thoại (<label class="text-red">*</label>):</label>
										<div class="col-sm-8">
											<div class="row">
												<div class="col-sm-8">
													<input type="number" class="form-control" id="phone" name="phone" v-model="employee.phone">		
												</div>
												<div class="col-sm-4">
													<button type="button" class="btn btn-warning" @click="checkNumberPhone()" >Kiểm tra</button>
												</div>
											</div>
										</div>
					                </div>
					                {{-- <div class="form-group">
										<label for="avatar" class="col-sm-4 control-label">Ảnh đại diện: </label>
										<div class="col-sm-8">
											<input type="file" id="avatar">
										</div>
					                </div> --}}
					                <div class="form-group">
										<label for="datepicker" class="col-sm-4 control-label">Ngày sinh (<label class="text-red">*</label>):</label>
										<div class="col-sm-8">
											<input type="text" class="form-control pull-right" id="datepicker" v-model="employee.birthday | moment 'DD/MM/YYYY'">
											{{-- | moment 'DD/MM/YYYY' --}}
										</div>
					                </div>
					                <div class="form-group">
										<label for="address" class="col-sm-4 control-label">Địa chỉ (<label class="text-red">*</label>):</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="address" placeholder="" v-model="employee.address">
										</div>
					                </div>
					                <div class="form-group">
										<label for="salary" class="col-sm-4 control-label">Lương cơ bản (<label class="text-red">*</label>):</label>
										<div class="col-sm-8">
											<input type="number" class="form-control" id="salary" placeholder="" v-model="employee.salary">
										</div>
					                </div>
					           	</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Thoát</button>
						<button v-if="!is_edit" type="button" class="btn btn-primary" @click="createInfoEmployee()">Thêm</button>
						<button v-if="is_edit" type="button" class="btn btn-primary" @click="updateInfoEmployee(is_service)">Lưu</button>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
	<script src="{{asset('/public/dist/js/moment.min.js')}}"></script>
	<script src="{{asset('/public/dist/js/employee.js')}}"></script>
	<script>
		$(document).ready(function(){
			$('#datepicker').datepicker({
			    format: 'dd/mm/yyyy',
			    // todayBtn: "linked",
			    // keyboardNavigation: false,
			    // forceParse: false,
			    // calendarWeeks: false,
			    // autoclose: true,
			});
		});
  	</script>
@endsection
