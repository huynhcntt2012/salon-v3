@extends('admin.main')
@section('css')
    <link href="{{asset('/public/asset/css/mainblog.css')}}" rel="stylesheet" type="text/css"/>
   
@stop
@section('js')
    <script src="{{asset('/public/asset/js/mainblog.js')}}" type="text/javascript"></script>    
    
@stop
@section('content')

<div class="row">   
        <div class="col-lg-9">
            <h2>Create blog</h2>
            <form action="{{url('createBlogPost')}}" enctype="multipart/form-data" method="POST">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" value='{{$post[0]->post_title or ""}}' name="title" id="title">
                </div>
                <button type="submit" class="btn btn-primary">save</button>
            </form>
        </div>
        <div  class="col-lg-3">
            <h2>Image represent</h2>
            <div class="box-img-rep">
                <img id="img_represent" src="{{asset('/public/images/'.(isset($post[0]->post_image)?$post[0]->post_image:'noimage.jpg').'')}}" class="img-responsive" alt="Cinque Terre">
            </div>            
            <br>
            <button class="btn btn-default" data-toggle="modal" data-target="#myModal">Set Image</button>
        </div>
    
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Library image <span class="status-upload">Upload Fail</span></h4>    
           
      </div>        
          </div>         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="setImgS()">Save changes</button>
      </div>
    </div>
  </div>
</div>
@stop