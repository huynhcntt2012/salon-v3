@extends('admin.main')
@section('css')
    <link href="../public/asset/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="../public/asset/css/typeahead.css" rel="stylesheet">
    <link href="../public/asset/css/checkout.css" rel="stylesheet">
    <link href="../public/asset/css/report.css" rel="stylesheet">
    <link href="../public/asset/css/common.css" rel="stylesheet">
@stop
@section('js')
	<script src="{{asset('/public/dist/js/contact.js')}}"></script>
@endsection
@section('content')

<section id="contact">
<div class="modal fade" id="conform" tabindex="-1" role="dialog">
    <div class="modal-dialog">
    	<div class="modal-content">
    		<div class="modal-header">
    			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    				<span aria-hidden="true">×</span>
    			</button>
                <h4 class="modal-title">Bạn muốn xóa phản hồi của <strong id="nass" ng-model="add_name">@{{ add_name }}</strong></h4>
    		</div>
    		<div class="modal-footer">
                <div id="revenue-footer" class="col-lg-12">
    				<button type="button" class="col-lg-5 btn btn-default pull-left" data-dismiss="modal">Thoát</button>
    				<button type="button" class="col-lg-5 btn btn-default pull-right"  @click="delContact()">Xóa</button>
                </div>
    		</div>
    	</div>
    </div>
</div>
                
<div class="modal fade" id="add-category" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Phản hồi khách hàng</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-horizontal">
								<div class="form-group">
									<label for="name-category" class="col-sm-12 control-label" style="text-align: left;">Tên Khách hàng</label>
									<div class="col-sm-12">
										Nguyễn Văn A
									</div>
				                </div>
                                <div class="form-group">
									<label for="name-category" class="col-sm-12 control-label" style="text-align: left;">Nội dung</label>
									<div class="col-sm-12">
                                        <div  id="message" placeholder="" v-model="add_message"> @{{add_message}}</div>
									</div>
				                </div>
				           	</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button v-if="iscontact" type="button" class="btn btn-default pull-left" data-dismiss="modal" @click="editContact()">Đã Xem</button>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Thoát</button>
				</div>
			</div>
    </div>
</div>
<div id="page-wrapper">  
        <div class="row">
        <div class="span12 columns">
        <div class="dataTables_wrapper no-footer" data-type="limit-offset">
        <div class="row"> 
            <div class="page-size col-md-6 dataTables_filter" align="right">
               <div id="search-order">
                    <label>Tìm kiếm:<input class="data-search-input" v-model="add_keyword" name="add_keyword" id="add_keyword" type="text" v-on:change="changeHandler"></label>
              </div>               
            </div>
        </div>
          <table id="customer_table" class="table table-hover table-bordered table-responsive table-striped" data-type="limit-offset">
            <thead>
              <tr>
                <th class="header col-lg-1" >STT</th>          
                <th class="header col-lg-3" >Tên khách hàng</th>
                <th class="header col-lg-4">Nội dung</th>
                <th class="header col-lg-2" >Ngày gởi</th>
                <th class="header col-lg-1" >Đã xem</th>
                <th class="header col-lg-1" >Xóa</th>                
              </tr>
            </thead>
            <tbody>
            <tr v-for="contact in contacts">
                <td class="header col-lg-1" >@{{ $index + 1 }}</td>          
                <td class="header col-lg-3" > @{{ contact.name }}</td>
                <td class="header col-lg-4">@{{ contact.message }}</td>
                <td class="header col-lg-2" >@{{ contact.created_at }}</td>
                <td class="header col-lg-1" >
                    <div v-if="contact.status == 1" style="cursor: pointer; cursor: hand; "><a style="text-decoration: none; cursor: hand; color: black;" data-toggle="modal" data-target="#add-category" @click="getfilterContact(contact.id,contact.name,contact.message,contact.status)">Đã xem</a></div>
                    <div v-if="contact.status == 0" style="cursor: pointer; cursor: hand; "><a style="text-decoration: none; cursor: hand; color: lime;" data-toggle="modal" data-target="#add-category" @click="getfilterContact(contact.id,contact.name,contact.message,contact.status)">Chưa xem</a></div>
                </td>
                <td class="header col-lg-1">
                    <div style="cursor: pointer; cursor: hand; "><a style="text-decoration: none; cursor: hand; color: red;" data-toggle="modal" data-target="#conform" @click="getfilterContact(contact.id,contact.name,contact.message,contact.status)">Xóa</a></div>
                </td>                
              </tr>  
            </tbody>
          </table>
          <div class="box-footer clearfix no-border">
				<div class="box-tools pull-right">
					<ul class="pagination pagination-sm inline">
						<li @click="getContact(1)"><a>&laquo;</a></li>
						<li v-for="page in contact.total_page"  
							@click="getContact(page + 1)"
							:class="((page + 1) == category.current_page) ? 'active' : ''" 
							><a>@{{ page + 1 }}</a></li>
						<li @click="getContact(category.last_page)" ><a>&raquo;</a></li>
					</ul>
				</div>
            </div> 
        </div> 
           
      </div>
</div><!-- /#page-wrapper -->   
    </div>
    </section>
    
        
@stop


