@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

  
      <div id="content" class="site-content">
    <div id="content" class="site-content container">
    	<div class="breadcrumbs">
            <div class="container">
                <div class="breadcrumbs__title"></div>
                <div class="breadcrumbs__items">
                	<div class="breadcrumbs__content">
                    	<div class="breadcrumbs__wrap">
                            <div class="breadcrumbs__item">
                                <a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a>
                            </div>
                            <div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div>
                            <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">Services</span></div>
                    	</div>
                	</div>
                </div>
                <div class="clear"></div>
            </div>
        	</div>
            
        	<div class="site-content_wrap container">
            	<div class="row">
                	<div id="primary" class="col-xs-12 col-md-12">
                    	<main id="main" class="site-main" role="main">
                        	<article id="post-412" class="post-412 page type-page status-publish hentry no-thumb">
                            	<header class="entry-header">
                            	   <h1 class="entry-title screen-reader-text">Services</h1>
                                </header> 
                            	<div class="entry-content">
                                	<div class="tm_builder_outer_content" id="tm_builder_outer_content">
                                    	<div class="tm_builder_inner_content tm_pb_gutters3">
                                        	<div class="tm_pb_section  tm_pb_section_0 tm_section_regular tm_section_transparent">
                                            	<div class="container">
                                            	<div class=" row tm_pb_row tm_pb_row_0">
                                            	<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_0 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            	<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_center main-title tm_pb_text_0">
                                            	<h2 style="text-align: center;">Our <em>Services</em></h2>
                                            	</div>  
                                            	</div>  
                                            	</div>  
                                            	</div>
                                            <div class="container">
                                            	<div class=" row tm_pb_row tm_pb_row_1">
                                                
                                                    <?php foreach($data as $post){ ?>
                                                	<div class="tm_pb_column tm_pb_column_1_3  tm_pb_column_1 col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                    	<div class="tm_pb_link_box tm_pb_link_box_0 tm_pb_module">
                                                            <div class="tm_pb_link_box_wrap">
                                                        	<figure>
                                                                <img src="public/images/<?php echo $post->post_image ?>" alt="">
                                                            </figure>
                                                        	<div class="tm_pb_link_box_content">
                                                            	<h3 class="tm_pb_link_box_title"><a href="detail/<?php echo $post->ID ?>"><?php echo $post->post_title ?></a></h3> <div class="tm_pb_blurb_content"> </div>
                                                            	<div class="tm_pb_link_box_button_holder"><a class="tm_pb_button" href="https://ld-wp.template-help.com/wordpress_58991/services/haircuts/">Service Details</a></div>
                                                        	</div>
                                                        	</div>  
                                                    	</div> 
                                                	</div>
                                                    
                                                    <?php } ?>
                                                    
                                                    
                                                </div>  
                                        	</div>
                                            
                                            <div class="container">
                                            	<div class=" row tm_pb_row tm_pb_row_3">
                                                	<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_7 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                	   <hr class="tm_pb_module tm_pb_space tm_pb_divider_0">
                                                	</div>  
                                            	</div>  
                                        	</div>
                                        	</div>  
                                    	</div>
                                	</div>
                                </div> 
                        	<footer class="entry-footer">
                        	</footer> 
                        	</article> 
                    	</main> 
                	</div> 
            	</div> 
    	</div>
         
   	</div>
</div>
        
@stop



