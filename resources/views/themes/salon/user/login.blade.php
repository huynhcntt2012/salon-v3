@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

  <div id="content" class="site-content">
		      <div class="breadcrumbs"><div class="container"><div class="breadcrumbs__title"></div><div class="breadcrumbs__items">
			<div class="breadcrumbs__content">
			<div class="breadcrumbs__wrap"><div class="breadcrumbs__item"><a href="#" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
			<div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div> <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">Login</span></div>
			</div>
			</div></div><div class="clear"></div></div>
			</div>
			<div class="site-content_wrap container">
			<div class="row">
			<div id="primary" class="col-md-12 col-lg-9">
			<main id="main" class="site-main" role="main">
			<header>
			<h1 class="page-title screen-reader-text">Login</h1>
			</header>
			<article id="post-77" class="entry author- post-77 page type-page status-publish" itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
			<header class="entry-header">
			<h1 class="entry-title font-headlines" itemprop="headline">Login</h1>
			</header>
			<div class="entry-content" itemprop="text">
				<div class="tml tml-login" id="theme-my-login">{{ $arrayBase['message']}}
				<form name="loginform" id="loginform" action="login" method="post">
				<p class="tml-user-login-wrap"> 
				<label for="user_login" class="font-secondary label-text">Số Điện Thoại</label> 
				<input type="text" name="user_login" id="user_login" class="input font-primary" value="" size="20">
				</p>
				<p class="tml-user-pass-wrap"> 
				<label for="user_pass" class="font-secondary label-password">Mật Khẩu</label> 
				<input type="password" name="user_pass" id="user_pass" class="input font-primary" value="" size="20" autocomplete="off">
				</p>
				<input type="hidden" name="_wp_original_http_referer" value="" class="font-primary">
				<div class="tml-rememberme-submit-wrap"><p class="tml-rememberme-wrap"> 
				<input name="rememberme" type="checkbox" id="rememberme" value="forever" class="font-primary"> 
				<label for="rememberme" class="label-checkbox font-primary">Remember Me</label>
				</p>
				<p class="tml-submit-wrap">
				<input type="submit" name="wp-submit" id="wp-submit" value="Log In" class="font-primary font-secondary"> 
				<input type="hidden" name="redirect_to" value="http://freewptp.com" class="font-primary">
				<input type="hidden" name="instance" value="" class="font-primary"> 
				<input type="hidden" name="action" value="login" class="font-primary">
				</p>
				</div>
				</form>
				<ul class="tml-action-links">
				<li><a href="register" rel="nofollow">Register</a></li>
				<li><a href="lostpassword" rel="nofollow">Lost Password</a></li>
				</ul>
				</div>
			</div>
			<footer class="entry-footer font-secondary"></footer>
			</article>
			
			</main> 
                
            </div> 
        @include('themes.'.$arrayBase['themes'].'.menuleft')
        </div> 
        
    </div> 
      
        
@stop



		
		