@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

           <div class="panel blank-panel">
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-1">
						    <div id="gallery-1" style="display:none;">
                                <?php
                                    foreach($data as $data){
                                    $link = explode('?v=', $data->link_youtube);
                                    ?>
        								<img alt="Youtube Video"
        									 data-type="youtube"
        									 data-videoid="<?php if(isset($link[1])){ echo $link[1]; } ?>"
        									 data-description="You can include youtube videos easily!">
                                <?php
                                    }
                                ?>
							 
							</div>
                        </div>
                    </div>
                </div>
            </div>
    
<script src="{{asset('public/asset/js/unitegallery.js')}}"></script>
<script src="{{asset('public/asset/js/ug-theme-tiles.js')}}"></script>
	<script type="text/javascript">
		var position = 1;
		$(document).ready(function(){
			$("#gallery-1").unitegallery({
				gallery_theme: "tiles",
				tiles_type:"nested",
				tile_enable_border: true,
                tile_border_width: 0,
                tile_border_color: "#ffffff",
                tile_border_radius: 5,
                tiles_space_between_cols: 10,
			});
		});
		function getPosition(is) {
			this.callGallery(is);
			console.log(is);
		}

		function callGallery(is) {
			var isGallery = document.querySelector('#gallery-' + is).classList.contains('ug-gallery-wrapper');
			if (!isGallery) {
				$("#gallery-" + is).unitegallery({
					gallery_theme: "tiles",
					tiles_type:"nested",
					tile_enable_border: true,
	                tile_border_width: 0,
	                tile_border_color: "#ffffff",
	                tile_border_radius: 5,
	                tiles_space_between_cols: 10,
				});
			}
		}
	</script>
      
        
@stop
