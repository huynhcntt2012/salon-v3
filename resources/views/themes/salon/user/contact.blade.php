@extends('themes.salon.index')
@section('css')

@stop
@section('js')

<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdU4upE53vnKyDgbXiL_c0NWHYl70IgbE"
	type="text/javascript"></script>
    
@stop
@section('content')

         

        
<div class="breadcrumbs"><div class="container"><div class="breadcrumbs__title"></div><div class="breadcrumbs__items">
		<div class="breadcrumbs__content">
		<div class="breadcrumbs__wrap"><div class="breadcrumbs__item"><a href="" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
		<div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div> <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">Contacts</span></div>
		</div>
		</div></div><div class="clear"></div></div>
		</div>
		<div class="site-content_wrap">
			<div class="row">
			<div id="primary" class="col-xs-12 col-md-12">
			<main id="main" class="site-main" role="main">
			<article id="post-713" class="post-713 page type-page status-publish hentry no-thumb">
			<header class="entry-header">
			<h1 class="entry-title screen-reader-text">Contacts</h1> </header> 
			<div class="entry-content">
			<div class="tm_builder_outer_content" id="tm_builder_outer_content">
			<div class="tm_builder_inner_content tm_pb_gutters3">
			<div class="tm_pb_section  tm_pb_section_0 tm_section_regular tm_section_transparent">
			<div class="container">
			<div class=" row tm_pb_row tm_pb_row_0">
			<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_0 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left main-title tm_pb_text_0">
			<h2 style="text-align: center;">Our <em>Location</em></h2>
			</div>  <div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_1">
			<h4 style="text-align: center;">Visit the best hairdressing salon in New York!</h4>
			<p style="text-align: center;">425 W. 14th Street, 2nd FloorNew York, New York 10014</p>
			</div>  
			<hr class="tm_pb_module tm_pb_space tm_pb_divider_0">
			<div class="tm_pb_module tm_pb_map_container  tm_pb_map_0">
			<div class="tm_pb_map" id="map">
			<div draggable="false" title="Hiển thị bản đồ phố với địa hình" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Địa hình</label></div></div></div>
			
			</div>
			</div>
			</div>
			</div>
			
			</div>
			</div>  
			</div>  
			</div>
			</div>  <div class="tm_pb_section  tm_pb_section_1 tm_pb_with_background tm_section_regular">
			<div class="container">
			<div class=" row tm_pb_row tm_pb_row_1">
			<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_1 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left main-title tm_pb_text_2">
			<h2 style="text-align: center;">Contact Us <em>Today!</em></h2>
			</div>  <div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_3">
			<h4 style="text-align: center;">Get in touch with us regarding any questions on our services<br>
			or simply to book an appointment!</h4>
			</div>  
			</div>  
			</div>  
			</div><div class="container">
			<div class=" row tm_pb_row tm_pb_row_2">
			<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_2 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div id="tm_pb_contact_form_0" class="tm_pb_contact_form tm_pb_contact_form_0 tm_pb_contact_form_container clearfix tm_pb_module" data-form_unique_num="0"><h2 class="tm_pb_contact_main_title"></h2>
			<form class="tm_pb_contact_form clearfix" method="post" action="contact" onsubmit="return checkContact();">
			<div class="tm-pb-contact-message"></div>
			<div class="row">
			<div class="tm_pb_contact_field col-md-12 ">
			<label  class="tm_pb_contact_form_label">Tên:</label>
			<input type="text" id="tm_pb_contact_name_1" name="tm_pb_contact_name_1" class="tm_pb_contact_form_input" value=""  data-required_mark="required" data-field_type="input" data-original_id="name" data-original_title="Tên:"></div><div class="tm_pb_contact_field col-md-12 ">
			<label  class="tm_pb_contact_form_label">Số Điện Thoại:</label>
			<input type="text" id="tm_pb_contact_phone_1" name="tm_pb_contact_phone_1" class="tm_pb_contact_form_input" value="" data-required_mark="required" data-field_type="input" data-original_id="input" data-original_title="Số Điện Thoại:"></div><div class="tm_pb_contact_field col-md-12 ">
			<label class="tm_pb_contact_form_label">Góp Ý:</label>
			<textarea name="tm_pb_contact_message_1" id="tm_pb_contact_message_1" class="tm_pb_contact_message tm_pb_contact_form_input" data-required_mark="required" data-field_type="text" data-original_id="message" data-original_title="Góp ý:"></textarea></div>
			</div>
			<input type="hidden" value="tm_contact_proccess" name="tm_pb_contactform_submit_0">
			<input type="text" value="" name="tm_pb_contactform_validate_0" class="tm_pb_contactform_validate_field">
			<div class="tm_contact_bottom_container">
			<div class="tm_pb_contact_right">
			<div class="clearfix">
			 <div class="cptch_block"> <span id="cptch_time_limit_notice_61" class="cptch_time_limit_notice cptch_to_remove" style="display: block;"></span>
                                    <span class="cptch_wrap">
                                        <label  class="cptch_label font-secondary label-text label-hidden" for="cptch_input_61"> 
                                            <span id="paraa" name="paraa" class="cptch_span">10</span> 
                                            <span class="cptch_span">&nbsp;+&nbsp;</span> <span class="cptch_span">
                                            <input  id="cptch_input_61" class="cptch_input  font-primary" type="text" name="cptch_number" value="" maxlength="1" size="1" aria-required="true" required="required" style="margin-bottom:0;display:inline;font-size: 12px;width: 40px;"></span> 
                                            <span class="cptch_span">&nbsp;=&nbsp;</span> 
                                            <span id="result" name="result" class="cptch_span">10</span> 
                                            <input type="hidden" name="cptch_result" value="RBk=" class="font-primary"> 
                                            <input type="hidden" name="cptch_time" value="1486923519" class="font-primary"> 
                                            <input type="hidden" name="cptch_form" value="wp_register" class="font-primary"> 
                                        </label>
                                        <span class="cptch_reload_button_wrap hide-if-no-js">
                                            <noscript>&lt;style type="text/css"&gt;.hide-if-no-js {
                                            					display: none !important;
                                            				}&lt;/style&gt;
                                            </noscript> 
                                            <span class="btnrefresh glyphicon glyphicon-refresh"></span> 
                                        </span>
                                    </span>
                                </div>
             <!--<span class="tm_pb_contact_captcha_question">12 + 9</span> = <input type="text" size="2" class="tm_pb_contact_captcha" data-original_title="Captcha" data-first_digit="12" data-second_digit="9" value="" name="tm_pb_contact_captcha_0" data-required_mark="required">-->
			</div>
			</div>  
			<button type="submit" class="tm_pb_contact_submit tm_pb_button">Submit</button> </div>
			<input type="hidden" id="_wpnonce-tm-pb-contact-form-submitted" name="_wpnonce-tm-pb-contact-form-submitted" value="f4ac0d03aa"><input type="hidden" name="_wp_http_referer" value=""></form></div> 
			</div>  
			</div>  
			</div>
			</div>  
			</div>
			</div> </div> 
			<footer class="entry-footer">
			</footer> 
			</article> 
			</main> 
			</div> 
			</div> 
			</div>
		</div>
        <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdU4upE53vnKyDgbXiL_c0NWHYl70IgbE"
	type="text/javascript"></script>
        <script>
		// This example displays a marker at the center of Australia.
		// When the user clicks the marker, an info window opens.
		//10.7728283,106.6705893
		function initMap() {
		  var uluru = {lat: 10.7728283, lng:106.6705893};
		  var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 18,
			center: uluru
		  });

		  var contentString = '<div id="content">'+
			  '<div id="siteNotice">'+
			  '</div>'+
			  '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
			  '<div id="bodyContent">'+
			  '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
			  'sandstone rock formation in the southern part of the '+
			  'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
			  'south west of the nearest large town, Alice Springs; 450&#160;km '+
			  '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
			  'features of the Uluru - Kata Tjuta National Park. Uluru is '+
			  'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
			  'Aboriginal people of the area. It has many springs, waterholes, '+
			  'rock caves and ancient paintings. Uluru is listed as a World '+
			  'Heritage Site.</p>'+
			  '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
			  'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
			  '(last visited June 22, 2009).</p>'+
			  '</div>'+
			  '</div>';

		  var infowindow = new google.maps.InfoWindow({
			content: contentString
		  });

		  var marker = new google.maps.Marker({
			position: uluru,
			map: map,
			title: 'Uluru (Ayers Rock)'
		  });
		  marker.addListener('click', function() {
			infowindow.open(map, marker);
		  });
		}
		google.maps.event.addDomListener(window,'load',initMap);
	</script>
      
        
@stop


