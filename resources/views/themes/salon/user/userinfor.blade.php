@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs__title"></div>
        <div class="breadcrumbs__items">
			<div class="breadcrumbs__content">
			<div class="breadcrumbs__wrap">
                <div class="breadcrumbs__item">
                    <a href="home" rel="home" title="Home">Home</a></div>
                    <div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div>
                    <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">User info</span></div>
			</div>
			</div>
        </div>
        <div class="clear"></div>
   </div>
</div>
<div class="site-content_wrap container">
    <div class="row">
        <div id="primary" class="col-md-12 col-lg-9">
            <section>
        		<div class="profile-head">
            		<div class="col-md- col-sm-4 col-xs-12">
                        <img src="public/images/<?php echo $users->image ?>" class="img-responsive" />
                        <h6></h6>
                    </div><!--col-md-4 col-sm-4 col-xs-12 close-->
            		<div class="col-md-5 col-sm-5 col-xs-12">
                		<h5>{{$users->name}}</h5>
                		<p></p>
                		<ul>
                    		<li>
                                <span class="glyphicon glyphicon-briefcase"></span> <?php echo date("d-m-Y", strtotime($users->birthday)); ?>
                            </li>
                    		<li>
                                <span class="glyphicon glyphicon-map-marker"></span> VN
                            </li>
                    		<li>
                                <span class="glyphicon glyphicon-home"></span> {{$users->address}}
                            </li>
                    		<li>
                                <span class="glyphicon glyphicon-phone"></span> <a href="#" title="call">{{$users->phone}}</a>
                            </li>
                            <li>
                                <span class="glyphicon fa fa-fw fa-money"></span> <a href="#" title="call">{{$users->point_use}}</a>
                            </li>
                    		<li>
                                <span class="glyphicon fa fa-fw fa-users"></span> <a href="#" title="call">{{$users->point_gift}}</a>
                            </li>
                            <li>
                                <span class="glyphicon fa fa-fw fa-users"></span> <a href="#" title="call">{{$users->nameRank}}</a>
                            </li>
                		</ul>
            		</div><!--col-md-8 col-sm-8 col-xs-12 close-->
        		</div><!--profile-head close-->
    		<!--container close-->
        		<div id="sticky">
        			<!-- Nav tabs -->
        			<ul class="nav nav-tabs nav-menu" role="tablist">
        			  <li class="active">
        				  <a href="#profile" role="tab" data-toggle="tab">
        					  <i class="fa fa-male"></i> Profile
        				  </a>
        			  </li>
        			  <li><a href="#change" role="tab" data-toggle="tab">
        				  <i class="fa fa-key"></i> Edit Profile
        				  </a>
        			  </li>
                      <li><a href="logout">
        				  <i class="fa fa-key"></i> LogOut
        				  </a>
        			  </li>
        			</ul><!--nav-tabs close-->
        			
        			<!-- Tab panes -->
        			<div class="tab-content">
            			<div class="tab-pane fade active in" id="profile">
            			<div class="container">
                    		<br clear="all" />
                    		<div class="row"></div><!--row close-->
                		</div><!--container close-->
                        </div><!--tab-pane close-->  	  
                        <div class="tab-pane fade" id="change">
                            <div class="container fom-main">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="register">Cập Nhật Thông Tin</h2>
                                    </div><!--col-sm-12 close-->
                                </div><!--row close-->
                                <br />
                                <div class="row">
                                    <form class="form-horizontal main_form text-left" action="changepass" method="post"  id="contact_form">
                                        <fieldset>
                                    		<div class="form-group col-md-12">
                                    		  <label class="col-md-10 control-label">Đổi Mật Khẩu</label>  
                                    		  <div class="col-md-12 inputGroupContainer">
                                        		  <div class="input-group">
                                        		      <input  name="pass" id="pass" placeholder="mật khẩu" class="form-control"  type="password">
                                                  </div>
                                    		  </div>
                                    		</div>
                                    		<div class="form-group col-md-12">
                                                <label class="col-md-10 control-label">Xác Nhận Mật Khẩu</label>  
                                                <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                <input  name="confpass" name="confpass" placeholder="xác nhận mật khẩu" class="form-control"  type="password">
                                                </div>
                                                </div>
                                    		</div>
                                		<!-- upload profile picture -->					
                                		<!-- Button -->
                                            <div class="form-group col-md-12">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-warning submit-button" >Save</button>
                                                    <button type="submit" class="btn btn-warning submit-button" >Cancel</button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div><!--row close-->
                            </div><!--container close -->          
                        </div><!--tab-pane close-->
                    </div><!--tab-content close-->
                </div><!--container close-->
            </section><!--section close-->
        </div>  
        @include('themes.'.$arrayBase['themes'].'.menuleft')
    </div> 
</div>
        
@stop