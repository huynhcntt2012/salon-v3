Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#device',
	data: {
		dateFrom: '19-04-2017',
        dateTo: '19-04-2017',
        customerphone: [],
        isdevice: {
			current_page: 1,
			total_page: 0,
			last_page: 0,
			total: 0
		},
        isall: false,
        content: ""
        
	},
	ready: function() {
	   this.getdevice();
	},
	methods: {
	   setDateNow: function() {
	        var vmThis = this;
            if(vmThis.pareDate(this.dateTo) < vmThis.pareDate(this.dateFrom)){
                this.dateTo = this.dateFrom;
            }else{
                today = new Date();
                if((today.getMonth()+1) < 10 )
                    month = '0' + (today.getMonth()+1);
                else
                    month = (today.getMonth()+1);
                
                date = today.getDate() +'-'+ month +'-'+ today.getFullYear();
                this.dateFrom = date;
                this.dateTo = date;
            }
            
		}
        ,
        checkSend: function(isSend,id) {
            this.id = id;
            this.content = "";
			switch(isSend) {
				case 1: 
					this.isall = false;
					break;
				case 2:
					this.isall =  true;
					break;
				default:
					this.isall = false;
					break;
			}
		},
        pareDate: function(str){
            str = str.split('-');
            dateFormat = str[2]+str[1]+str[0];
            return dateFormat;
        },
        getdevice: function(page){
            var vmThis = this;
            if (!page) {
				page = 1;
			}
            this.$http.get(`apiPhone/getdevice?page=${page}`)
				.then(function(response) {
				    vmThis.isdevice= {
						current_page: response.data.current_page,
						total_page: Math.ceil(response.data.total/response.data.per_page),
						last_page: response.data.last_page,
						total: response.data.total
					};
                    Vue.set(vmThis, 'devicePage', vmThis.isdevice);
                    Vue.set(this, 'device', response.data.data);
                    
                    
				})
				.catch(function(error) {
					console.log(error);
				})
        },
       	switchPage: function(page) {
			this.getdevice(page);
		},
        sendnotification: function() {
            if(this.content == "") {
                toastr.error('Chưa có nội dung', 'Nội dung thông báo');
                return;
            }
			data = {
				content: this.content,
				id: this.id,
				isall: this.isall
			}

			this.$http.post('apiPhone/sendnotifition', data)
				.then(function(response) {
					if (response.status == 200) {
						toastr.success('Gửi thông báo thành công', 'Thao tác thành công');
						$('#add-category').modal('hide');
					}
				})
				.catch(function(error) {
					toastr.error('Thông báo kỹ thuật ngay 1', 'Lỗi chưa xác định');
					console.log(error);
				})
		},
        
        
	},
    watch: {
        dateTo: function(val, oldVal) {
          // change of userinput, do something
            var vmThis = this;
            if(vmThis.pareDate(this.dateTo) < vmThis.pareDate(this.dateFrom)){
                this.dateFrom = this.dateTo;
            }
        },
        dateFrom: function(val, oldVal) {
          // change of userinput, do something
            var vmThis = this;
            if(vmThis.pareDate(this.dateTo) < vmThis.pareDate(this.dateFrom)){
                this.dateTo = this.dateFrom;
            }
        }
  }
})