Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#revenue',
	data: {
		dateFrom: '19-04-2017',
        dateTo: '19-04-2017',
        customerphone: [],
	},
	ready: function() {
	   this.getnow();
       this.getEmployee();
       this.getCustomer();
       this.getService();
	},
	methods: {
	   getService: function(){
	       today = new Date();
                if((today.getMonth()) < 10 )
                    month = '0' + (today.getMonth());
                else
                    month = (today.getMonth());
                
                date = today.getFullYear()  +'-'+ month + '-' + today.getDate();
	       data = {
				start: date,
				end: date
			}
	       this.$http.post(`api/revenue/getservice`,data)
				.then(function(response) {
					Vue.set(this,'service',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
	   }
	   ,
       getServiceDate: function(){
	       data = {
				start: this.pareDate(this.dateFrom),
				end: this.pareDate(this.dateTo)
			}
	       this.$http.post(`api/revenue/getservice`,data)
				.then(function(response) {
					Vue.set(this,'service',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
	   }
	   ,
	   getCustomer: function(){
	       today = new Date();
                if((today.getMonth()) < 10 )
                    month = '0' + (today.getMonth());
                else
                    month = (today.getMonth());
                
                date = today.getFullYear()  +'-'+ month + '-' + today.getDate();
	       data = {
				start: date,
				end: date
			}
	       this.$http.post(`api/revenue/customer`,data)
				.then(function(response) {
					Vue.set(this,'customer',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
	   }
	   ,
       getCustomerDate: function(){
	       data = {
				start: this.pareDate(this.dateFrom),
				end: this.pareDate(this.dateTo)
			}
	       this.$http.post(`api/revenue/customer`,data)
				.then(function(response) {
					Vue.set(this,'customer',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
	   }
	   ,
	   getEmployee: function(){
	       today = new Date();
                if((today.getMonth()+1) < 10 )
                    month = '0' + (today.getMonth()+1);
                else
                    month = (today.getMonth()+1);
                
                date = today.getFullYear()  +'-'+ month;
	       
	       this.$http.get(`api/serviceuse?date=${date}`)
				.then(function(response) {
					Vue.set(this,'employee',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
	   },
       getSalary: function(){
	       today = new Date();
                if((today.getMonth()+1) < 10 )
                    month = '0' + (today.getMonth()+1);
                else
                    month = (today.getMonth()+1);
                
                date = this.pareMonth(this.dateFrom);
	       this.$http.get(`api/serviceuse?date=${date}`)
				.then(function(response) {
					Vue.set(this,'employee',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
	   }
	   ,
	   getnow: function(){
	       today = new Date();
                if((today.getMonth()) < 10 )
                    month = '0' + (today.getMonth());
                else
                    month = (today.getMonth());
                
                date = today.getFullYear()  +'-'+ month + '-' + today.getDate();
	       data = {
				start: date,
				end: date
			}
	       this.$http.post(`api/revenue/getInfo`,data)
				.then(function(response) {
					Vue.set(this,'revenue',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
	   }
	   ,
       getDate: function(){
            data = {
				start: this.pareDate(this.dateFrom),
				end: this.pareDate(this.dateTo),
			}
            this.$http.post(`api/revenue/getInfo`,data)
				.then(function(response) {
					Vue.set(this,'revenue',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
	   }
       ,
	   setDateNow: function() {
	        var vmThis = this;
            if(vmThis.pareDate(this.dateTo) < vmThis.pareDate(this.dateFrom)){
                this.dateTo = this.dateFrom;
            }else{
                today = new Date();
                if((today.getMonth()) < 10 )
                    month = '0' + (today.getMonth());
                else
                    month = (today.getMonth());
                
                date = today.getDate() +'-'+ month +'-'+ today.getFullYear();
                this.dateFrom = date;
                this.dateTo = date;
            }
            
		}
        ,
        pareDate: function(str){
            str = str.split('-');
            dateFormat = str[2]+str[1]+str[0];
            return dateFormat;
        },
        pareMonth: function(str){
            str = str.split('-');
            dateFormat = str[1]+'-'+str[0];
            return dateFormat;
        }
	},
    watch: {
        dateTo: function(val, oldVal) {
          // change of userinput, do something
            var vmThis = this;
            if(vmThis.pareDate(this.dateTo) < vmThis.pareDate(this.dateFrom)){
                this.dateFrom = this.dateTo;
            }
        },
        dateFrom: function(val, oldVal) {
          // change of userinput, do something
            var vmThis = this;
            if(vmThis.pareDate(this.dateTo) < vmThis.pareDate(this.dateFrom)){
                this.dateTo = this.dateFrom;
            }
        }
  }
})