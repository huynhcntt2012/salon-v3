Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#employee',
	data: {
		category: [],
        service: [],
        add_id: '0',
        add_idperson: '0',
        idservice: '0',
        income: '3111',
        percent: '0'
	},
	ready: function() {
		//this.getCategory();
	},
	methods: {
		getCategory: function() {
			this.$http.get('api/getSettingService/'+this.add_idperson)
				.then(function(response) {
					Vue.set(this,'subs',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
            
		}
        ,
        getemployee: function() {
			this.$http.get('api/getSettingEmployee/'+this.add_id)
				.then(function(response) {
					Vue.set(this,'employee',response.data);
                    this.add_idperson = response.data[0].id;
                    this.getCategory();
				})
				.catch(function(error) {
					console.log(error);
				});
                
		},
        saveIncome: function(idservice,id_service){
            //alert("idService:"+idservice+"idPerson:"+this.add_idperson+"Imcome:"+document.getElementById('income'+id_service).value);
            income1 = document.getElementById('income'+id_service).value;
            percent = document.getElementById('percent'+id_service).value;
            this.income = income1;
            this.idservice = idservice;
            this.percent = percent;
            
            this.$http.post('api/setServiceEmployee/'+this.add_idperson+'/'+this.idservice+'/'+this.income+'/'+this.percent)
				.then(function(response) {
					if (response.status == 200) {
						//vmThis.fetchDataEmployee(vmThis.page.current);
						toastr.success('Tạo thành công', 'Thao tác thành công');
						//$('#add-employee').modal('hide');
                        this.getCategory();
					}
				})
				.catch(function(error) {
					toastr.error('Lỗi chưa xác nhận', 'Lỗi nha!');
				})
        }
	}
})