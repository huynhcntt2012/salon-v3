Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#customer',
	data: {
		customer: [],
        customerlook: [],
        customerphone: [],
	},
	ready: function() {
		this.getCustomer();
	},
	methods: {
		getCustomer: function() {
			this.$http.get('api/getCustomer')
				.then(function(response) {
					Vue.set(this,'customer',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
		},
        
        getCustomer1: function() {
			this.$http.get('api/getCustomer')
				.then(function(response) {
				    console.log('error');
					Vue.set(this,'customerlook',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
		},
        
        getSearch: function() {
            name = $("#name").val()
            phone = $("#phone").val()
			this.$http.get(`api/getSearch?name=${name}&phone=${phone}`)
				.then(function(response) {
				    
					Vue.set(this,'customerlook',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
		}
        ,
        getSearchDate: function() {
            date = $("#date").val()
            console.log(date);
			this.$http.get(`api/getSearchDate?date=${date}`)
				.then(function(response) {
				    
					Vue.set(this,'customerlook',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
		},
        getSearchPhone: function() {
            phone = $("#phone").val();
            
			this.$http.get(`api/getSearchPhone?phone=${phone}`)
				.then(function(response) {
					Vue.set(this,'customerphone',response.data);
				})
				.catch(function(error) {
					console.log(error);
				})
		},
        editcustomer: function(){
            toastr.success('Block thành công', 'Thao tác thành công');
        }
	}
})