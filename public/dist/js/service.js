Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#service',
	data: {
		categories: [],
		sub_categories: [],
		is_service: false,
		add_note_service: '',
		add_price_service: null,
		add_name_service: '',
		add_income_service: null,
		add_selected_service: null,
		is_edit: false,
		id_edit: null,
		category_id_selected: 0,
		category: {
			current_page: 1,
			total_page: null,
			last_page: null,
			total: null
		},
		sub: {
			current_page: 1,
			total_page: null,
			last_page: null,
			total: null
		},
		is_filter_sub: false,
	},
	ready: function() {
		this.getCategory();
		this.getSubCategory();
		Vue.set(this, 'is_service', this.is_service);
	},
	methods: {
		/**
		 * Clear or get value for input in modal
		 * @param  {[type]} name     input name
		 * @param  {[type]} note     textare note
		 * @param  {[type]} price    input price
		 * @param  {[type]} income   input income
		 * @param  {[type]} selected select category
		 * @return {[type]} clear value for input or get value for input
		 */
		resetFrom: function(name, note, price, income, selected) {
			if (note) {
				this.add_note_service = note;	
			} else {
				this.add_note_service = '';
			}
			
			if (price) {
				this.add_price_service = price;	
			} else {
				this.add_price_service = null;
			}
			
			if (name) {
				this.add_name_service = name;	
			} else {
				this.add_name_service = '';
			}
			
			if (income) {
				this.add_income_service = income;	
			} else {
				this.add_income_service = null;
			}

			if (selected) {
				this.add_selected_service = selected;
			} else {
				this.add_selected_service = null;
			}
			Vue.set(this, 'add_note_service', this.add_note_service);
			Vue.set(this, 'add_income_service', this.add_income_service);
			Vue.set(this, 'add_name_service', this.add_name_service);
			Vue.set(this, 'add_price_service', this.add_price_service);
			Vue.set(this, 'add_selected_service', this.add_selected_service);
		},
		/**
		 * Get data for category
		 * @param  {[type]} page Get paginate wish go to
		 * @return {[type]}      Array data object
		 */
		getCategory: function(page) {
			var vmThis = this;
			if (!page) {
				page = 1;
			}
			this.$http.get(`api/getCategory?page=${page}`)
				.then(function(response) {
					vmThis.category= {
						current_page: response.data.current_page,
						total_page: Math.ceil(response.data.total/response.data.per_page),
						last_page: response.data.last_page,
						total: response.data.total
					};
                    
					Vue.set(vmThis, 'category', vmThis.category);
					Vue.set(this,'categories',response.data.data)
				})
				.catch(function(error) {
					console.log(error);
				})
		},
		/**
		 * Get data for sub
		 * @param  {[type]} page Get paginate wish go to
		 * @return {[type]}      Array data object
		 */
		getSubCategory: function(page) {
			var vmThis = this;
			if (!page) {
				page = 1;
			}
			this.$http.get(`api/getSub?page=${page}`)
				.then(function(response) {
					vmThis.sub = {
						current_page: response.data.current_page,
						total_page: Math.ceil(response.data.total/response.data.per_page),
						last_page: response.data.last_page,
						total: response.data.total
					}
                    
					Vue.set(this, 'sub', vmThis.sub);
					Vue.set(this, 'sub_categories', response.data.data);
				})
				.catch(function(error) {
					console.log(error);
				})
		},
		/**
		 * Check current category or sub
		 * @param  {int} isService Number 1 is Category, 2 is Sub
		 * @return {[type]}            valiable is_service true/false
		 */
		checkService: function(isService) {
			this.is_edit = false;
			Vue.set(this, 'is_edit', this.is_edit);
			switch(isService) {
				case 1: 
					this.is_service = false;
					break;
				case 2:
					this.is_service =  true;
					break;
				default:
					this.is_service = false;
					break;
			}
			Vue.set(this, 'is_service', this.is_service);
		},
		/**
		 * Create category or sub
		 * @param  {Boolean} isService valiable Gobal check wish create category or sub
		 * @return {[type]}            [description]
		 */
		createService: function(isService) {
			if (isService) {
				if (this.add_name_service.replace(/\s+/g, ' ').length < 5) {
					toastr.error('Nhập tên phải trên 5 ký tự', 'Lỗi nha!');
					return false;
				}
				if (!this.add_selected_service) {
					toastr.error('Phải chọn danh mục', 'Lỗi nha!');
					return false;
				}
				if (this.add_price_service < 1) {
					toastr.error('Gía phải lớn hơn 0', 'Lỗi nha!');
					return false;
				}
				if (this.add_income_service < 1) {
					toastr.error('Hoa hồng phải lớn hơn 0', 'Lỗi nha!');
					return false;
				}
				if (this.add_note_service.replace(/\s+/g, ' ').length < 5) {
					toastr.error('Nhập ghi chú phải trên 5 ký tự', 'Lỗi nha!');
					return false;
				}
				var vmThis = this, data;
				data = {
					name: this.add_name_service,
					category: this.add_selected_service,
					price: this.add_price_service,
					income: this.add_income_service,
					note: this.add_note_service
				}

				this.$http.post('api/createSub', data)
					.then(function(response) {
						if (response.status == 200) {
							toastr.success('Tạo dịch vụ thành công', 'Thao tác thành công');
							$('#add-category').modal('hide');
							vmThis.resetFrom();
							vmThis.getSubCategory();
						}
					})
					.catch(function(error) {
						toastr.error('Thông báo kỹ thuật ngay 1', 'Lỗi chưa xác định');
						console.log(error);
					})
			} else {
				if (this.add_name_service.replace(/\s+/g, ' ').length < 5) {
					toastr.error('Nhập tên phải trên 5 ký tự', 'Lỗi nha!');
					return false;
				} 
				if (this.add_note_service.replace(/\s+/g, ' ').length < 5){
					toastr.error('Nhập ghi chú phải trên 5 ký tự', 'Lỗi nha!');
					return false;
				}
				var vmThis = this, data = {
					name: this.add_name_service,
					note: this.add_note_service
				}
				this.$http.post('api/createCategory', data)
					.then(function(response) {
						if(response.status == 200) {
							vmThis.getCategory();
							toastr.success('Tạo ngon lành', 'Thao tác thành công');
							$('#add-category').modal('hide');
							vmThis.resetFrom();
						}
					})
					.catch(function(error) {
						toastr.error('Thông báo kỹ thuật ngay 2', 'Lỗi chưa xác định');
					})
			}
		},
		/**
		 * Delete Category by id
		 * @param  {[type]} id Is id of category.
		 * @return {[type]}    [description]
		 */
		deleteCategory: function(id) {
			var vmThis = this;
			this.$http.delete(`api/delCategory/${id}`)
				.then(function(response) {
					if (response.status == 200) {
						vmThis.getCategory();
						toastr.success('Xóa thành công', 'Thao tác thành công');
					}
				})
				.catch(function(error) {
					console.log(error);
				})
		},
		/**
		 * Get single category by id.
		 * @param  {[type]} id is id of category.
		 * @return {[type]}    Array object
		 */
		getdataCategory: function(id) {
			var vmThis = this;
			this.is_edit = true;
			this.id_edit = id;
			Vue.set(this, 'is_edit', this.is_edit);
			this.$http.get(`api/category/${id}`)
				.then(function(response) {
					if (response.status == 200) {
						vmThis.resetFrom(response.data[0].name, response.data[0].note);
					}
				})
		},
		/**
		 * Update Category or sub
		 * @param  {Boolean} isService Check update category or update sub
		 * @return {[type]}            [description]
		 */
		editService: function(isService) {
			if (isService) {
				if (this.add_name_service.replace(/\s+/g, ' ').length < 5) {
					toastr.error('Nhập tên phải trên 5 ký tự', 'Lỗi nha!');
					return false;
				}
				if (!this.add_selected_service) {
					toastr.error('Phải chọn danh mục', 'Lỗi nha!');
					return false;
				}
				if (this.add_price_service < 1) {
					toastr.error('Gía phải lớn hơn 0', 'Lỗi nha!');
					return false;
				}
				if (this.add_income_service < 1) {
					toastr.error('Hoa hồng phải lớn hơn 0', 'Lỗi nha!');
					return false;
				}
				if (this.add_note_service.replace(/\s+/g, ' ').length < 5) {
					toastr.error('Nhập ghi chú phải trên 5 ký tự', 'Lỗi nha!');
					return false;
				}
				var vmThis = this, data;
				data = {
					name: this.add_name_service,
					category: this.add_selected_service,
					price: this.add_price_service,
					income: this.add_income_service,
					note: this.add_note_service
				}

				this.$http.put(`api/editSub/${this.id_edit}`, data)
					.then(function(response) {
						if (response.status == 200) {
							toastr.success('Cập nhật dịch vụ thành công', 'Thao tác thành công');
							$('#add-category').modal('hide');
							vmThis.resetFrom();
							vmThis.getSubCategory();
						}
					})
					.catch(function(error) {
						toastr.error('Thông báo kỹ thuật ngay 1', 'Lỗi chưa xác định');
						console.log(error);
					})
				
			} else {
				if (this.add_name_service.replace(/\s+/g, ' ').length < 5) {
					toastr.error('Nhập tên phải trên 5 ký tự', 'Lỗi nha!');
					return false;
				} 
				if (this.add_note_service.replace(/\s+/g, ' ').length < 5){
					toastr.error('Nhập ghi chú phải trên 5 ký tự', 'Lỗi nha!');
					return false;
				}
				var vmThis = this, data = {
					name: this.add_name_service,
					note: this.add_note_service
				}
				this.$http.put(`api/editCategory/${this.id_edit}`, data)
					.then(function(response) {
						if(response.status == 200) {
							vmThis.getCategory();
							toastr.success('Tạo ngon lành', 'Thao tác thành công');
							$('#add-category').modal('hide');
							vmThis.resetFrom();
						}
					})
					.catch(function(error) {
						toastr.error('Thông báo kỹ thuật ngay', 'Lỗi chưa xác định');
					})
			}
		},
		/**
		 * Detele sub by id
		 * @param  {[type]} id Is id of sub
		 * @return {[type]}    [description]
		 */
		deleteSubCategory: function(id) {
			var vmThis = this;
			this.$http.delete(`api/delSub/${id}`)
				.then(function(response) {
					if (response.status == 200) {
						vmThis.getSubCategory();
						toastr.success('Xóa thành công', 'Thao tác thành công');
					}
				})
				.catch(function(error) {
					console.log(error);
				})
		},
		/**
		 * Get single sub by id.
		 * @param  {[type]} id is of sub
		 * @return {[type]}    Array object
		 */
		getSingleSub: function(id) {
			this.checkService(2);
			this.is_edit = true;
			this.id_edit = id;
			Vue.set(this, 'is_edit', this.is_edit);
			for (var i = this.sub_categories.length - 1; i >= 0; i--) {
				if (this.sub_categories[i].id == id) {
					this.resetFrom(
						this.sub_categories[i].name, 
						this.sub_categories[i].note, 
						this.sub_categories[i].price, 
						this.sub_categories[i].income, 
						this.sub_categories[i].category
						);
				}
			}
		},
		/**
		 * Get Sub by category
		 * @param  {[type]} page is wish go to.
		 * @return {[type]}      Array object
		 */
		getSubByCategory: function(page) {
			var vmThis = this;
			if (!page) {
				page = 1;
			}

			if (this.category_id_selected == 0) {
				this.getSubCategory();
				this.is_filter_sub = false;
			} else {
				this.is_filter_sub = true;
				this.$http.get(`api/category/${this.category_id_selected}/sub?page=${page}`)
					.then(function(response) {
						if (response.status == 200) {
							vmThis.sub = {
								current_page: response.data.current_page,
								total_page: Math.ceil(response.data.total/response.data.per_page),
								last_page: response.data.last_page,
								total: response.data.total
							}
							Vue.set(this, 'sub', vmThis.sub);
							vmThis.sub_categories = response.data.data;
							Vue.set(this, 'sub_categories', this.sub_categories);
						}
					})
					.catch(function(error) {
						console.log(error);
					});
			}
		},
		/**
		 * Switch get all sub of get sub by category.
		 * @param  {[type]} page is wish go to.
		 * @return {[type]}      Array object
		 */
		switchGetPagaSub: function(page) {
			if (this.is_filter_sub) {
				this.getSubByCategory(page);
			} else {
				this.getSubCategory(page);
			}
		}
	}
})