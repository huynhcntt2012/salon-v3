Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#point',
	data: {
		point: [],
		ispoint: false,
		add_point: '',
		add_money: '',
        add_id: '',
		point_id_selected: 0,
	},
	ready: function() {
		this.getPoint();
        Vue.set(this, 'ispoint', this.ispoint);
	},
	methods: {
	   resetFrom: function(point, money) {
			if (point) {
				this.add_point= point;	
			} else {
				this.add_point = '';
			}
            
            if (money) {
				this.add_money= money;	
			} else {
				this.add_money = '';
			}
			Vue.set(this, 'add_point', this.add_point);
			Vue.set(this, 'add_money', this.add_money);
		},
		getPoint: function() {
			this.$http.get('api/getpoint')
				.then(function(response) {
					Vue.set(this,'point',response.data);
				})
				.catch(function(error) {
					console.log(error);
				})
		},
        createPoint: function(id) {
            if (this.add_point.replace(/\s+/g, ' ').length < 1) {
				toastr.error('Nhập tên phải trên 15 ký tự', 'Lỗi nha!');
				return false;
			}
            if (this.add_money.replace(/\s+/g, ' ').length < 5) {
				toastr.error('Nhập tên phải trên 5 ký tự', 'Lỗi nha!');
				return false;
			}
			var vmThis = this, data;
            data = {
				point:  this.add_point,
				money: this.add_money,
			}
            this.$http.post(`api/createpoint/${id}`, data)
    			.then(function(response) {
    				Vue.set(this,'point',response.data);
                    $('#add-category').modal('hide');
                    toastr.success('Cập nhật Thành công', 'Thành công!');
    			})
    			.catch(function(error) {
    				console.log(error);
    			})
		}
        
	}
})