Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
Vue.filter('moment', function () {
  	var args = Array.prototype.slice.call(arguments),
	value = args.shift(),
	date = moment(value);

	if (!date.isValid()) return '';

	function parse() {
		var args = Array.prototype.slice.call(arguments).map(function(str) { return str.replace(/^("|')|("|')$/g, ''); }),
		method = args.shift();

		switch (method) {

			case 'from':

				// Display a moment in relative time, either from now or from a specified date.
				// http://momentjs.com/docs/#/displaying/fromnow/

				var from = 'now';
				if (args[0] == 'now') args.shift();

				if (moment(args[0]).isValid()) {
					// If valid, assume it is a date we want the output computed against.
					from = moment(args.shift());
				}

				var removeSuffix = false;
				if (args[0] == 'true') {
					args.shift();
					var removeSuffix = true;
				}

				if (from != 'now') {
					date = date.from(from, removeSuffix);
					break;
				}

				date = date.fromNow(removeSuffix);
				break;

			default:
				// Format
				// Formats a date by taking a string of tokens and replacing them with their corresponding values.
				// http://momentjs.com/docs/#/displaying/format/

				var format = method;
				date = date.format(format);
		}

		if (args.length) parse.apply(parse, args);
	}

	parse.apply(parse, args);


	return date;
})
new Vue({
	el: '#employee',
	data: {
		employee_list: [],
		is_edit: false,
		id_employee: null,
		is_search: false,
		page: {
			current: null,
			last: null,
			total: null,
			total_page: null,
		},
		employee: {
			firstname: '',
			lastname: '',
			idperson: '',
			phone: '',
			password: '',
			birthday: '',
			address: '',
			salary: '',
		},
		keyword: '',
	},
	ready: function() {
		this.fetchDataEmployee(1);
	},
	methods: {
		fetchDataEmployee: function(page,check) {
			let vmThis = this;
			if (page != this.page.current) {
				if (!page) {
					page = 1;
				}
				this.$http.get(`api/employees?page=${page}`)
					.then(function(response) {
						vmThis.page = {
							current: response.data.current_page,
							last: response.data.last_page,
							total: response.data.total,
							total_page: Math.ceil(response.data.total/response.data.per_page)
						}
						Vue.set(vmThis, 'page', vmThis.page);
						vmThis.employee_list = response.data.data;
					})
					.catch(function(error) {
						console.log(error);
					})
			} else if (page == this.page.current && (check != 0)) {
				this.$http.get(`api/employees?page=${page}`)
					.then(function(response) {
						vmThis.page = {
							current: response.data.current_page,
							last: response.data.last_page,
							total: response.data.total,
							total_page: Math.ceil(response.data.total/response.data.per_page)
						}
						Vue.set(vmThis, 'page', vmThis.page);
						vmThis.employee_list = response.data.data;
					})
					.catch(function(error) {
						console.log(error);
					})
			}
		},
		createInfoEmployee: function() {
			if (!this.employee.firstname) {
				toastr.error('Họ chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.lastname) {
				toastr.error('Tên chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.idperson) {
				toastr.error('Chứng minh chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.phone) {
				toastr.error('Số điện thoại chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.birthday) {
				toastr.error('Ngày sinh chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.address) {
				toastr.error('Địa chỉ chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.salary) {
				toastr.error('Chưa nhập mức lương', 'Lỗi nha!');
				return false;
			}
			let vmThis = this, data = {
				firstname: this.employee.firstname,
				lastname: this.employee.lastname,
				idperson: this.employee.idperson,
				phone: this.employee.phone,
				// password: this.employee.password,
				birthday: this.employee.birthday,
				address: this.employee.address,
				salary: this.employee.salary
			}
			this.$http.post(`api/employee/create`,data)
				.then(function(response) {
					if (response.status == 200) {
						vmThis.fetchDataEmployee(vmThis.page.current);
						toastr.success('Tạo thành công', 'Thao tác thành công');
						$('#add-employee').modal('hide');
					}
				})
				.catch(function(error) {
					 if (error.status == 400) {
						toastr.error('Số điện thoại này đã tồn tại', 'Quá trình tạo lỗi');
						return false;
					} else {
						toastr.error('Lỗi chưa xác nhận', 'Lỗi nha!');
					}
				})
		},
		updateInfoEmployee: function() {
			if (!this.employee.firstname) {
				toastr.error('Họ chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.lastname) {
				toastr.error('Tên chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.idperson) {
				toastr.error('Chứng minh chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.phone) {
				toastr.error('Số điện thoại chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.birthday) {
				toastr.error('Ngày sinh chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.address) {
				toastr.error('Địa chỉ chưa nhập', 'Lỗi nha!');
				return false;
			}
			if (!this.employee.salary) {
				toastr.error('Chưa nhập mức lương', 'Lỗi nha!');
				return false;
			}
			let vmThis = this, data = {
				firstname: this.employee.firstname,
				lastname: this.employee.lastname,
				idperson: this.employee.idperson,
				phone: this.employee.phone,
                birthday: document.querySelector('#datepicker').value,
				//birthday: this.employee.birthday,
				address: this.employee.address,
				salary: this.employee.salary
			}
			this.$http.post(`api/employee/${this.id_employee}/edit`,data)
				.then(function(response) {
					if (response.status == 200) {
						vmThis.fetchDataEmployee(vmThis.page.current);
						toastr.success('Tạo thành công', 'Thao tác thành công');
						$('#add-employee').modal('hide');
					} else if (response.status == 400) {
						toastr.error('Số điện thoại này đã tồn tại', 'Quá trình tạo lỗi');
						return false;
					}
				})
				.catch(function(error) {
					toastr.error('Lỗi chưa xác nhận', 'Lỗi nha!');
				})
		},
		switchTemplate: function(number,id) {
			if (number == 0) {
				this.is_edit = false;
				this.resetForm();
			} else if (number == 1) {
				this.is_edit = true;
				this.id_employee = id;
				for (var i = 0; i < this.employee_list.length; i++) {
					if (this.employee_list[i].id == id) {
						this.resetForm(this.employee_list[i].firstname,this.employee_list[i].lastname,this.employee_list[i].idperson,this.employee_list[i].birthday,this.employee_list[i].address,this.employee_list[i].phone,this.employee_list[i].salary);
					}
				}
			}
			Vue.set(this,'is_edit', this.is_edit);
		},
		resetForm: function(firstname, lastname, idperson, birthday, address, phone,salary) {
			if (firstname) {
				this.employee.firstname = firstname;
			} else {
				this.employee.firstname = '';
			}
			if (lastname) {
				this.employee.lastname = lastname;	
			} else {
				this.employee.lastname = '';
			}
			if (idperson) {
				this.employee.idperson = idperson;
			} else {
				this.employee.idperson = '';
			}
			if (birthday) {
				this.employee.birthday = birthday;
			} else {
				this.employee.birthday = '';
			}
			if (address) {
				this.employee.address = address;
			} else {
				this.employee.address = '';
			}
			if (phone) {
				this.employee.phone = phone;
			} else {
				this.employee.phone = '';
			}
			if (salary) {
				this.employee.salary = salary;
			} else {
				this.employee.salary = '';
			}
			Vue.set(this, 'employee.firstname',this.employee.firstname);
			Vue.set(this, 'employee.lastname',this.employee.lastname);
			Vue.set(this, 'employee.phone',this.employee.phone);
			Vue.set(this, 'employee.birthday',this.employee.birthday);
			Vue.set(this, 'employee.idperson',this.employee.idperson);
			Vue.set(this, 'employee.address',this.employee.address);
			Vue.set(this, 'employee.salary',this.employee.salary);
		},
		getKeySearch: function() {
			var vmThis = this;
			if (this.keyword.replace(/\s+/g, ' ').length > 1) {
				this.$http.get(`api/employee/search?q=` + this.keyword)
					.then(function(response) {
						vmThis.employee_list = [];
						vmThis.employee_list = response.data;
						vmThis.is_search = true;
						Vue.set(vmThis, 'is_search',vmThis.is_search);
						Vue.set(vmThis, 'employee_list',vmThis.employee_list);
					})
					.catch(function(error) {
						console.log(error);
					})
			} else {
				if (this.keyword.replace(/\s+/g, ' ').length == 0) {
					this.is_search = false;
					Vue.set(this, 'is_search',this.is_search);
					this.fetchDataEmployee();
				}
			}
		},
		blockEmployee: function(id, status) {
			var vmThis = this;
			var data = {
				status: status
			}
			this.$http.post(`api/employee/${id}`, data)
				.then(function(response) {
					if (response.status == 200) {
						toastr.success('Block thành công', 'Thao tác thành công');
						vmThis.fetchDataEmployee(vmThis.page.current);
					}
				})
				.catch(function(error) {
					toastr.error('Block lỗi', 'Thao tác không thành công');
				})
		},
		checkNumberPhone: function() {
			var data = {
				phone: this.employee.phone
			}
			this.$http.post(`api/employee`, data)
				.then(response => {
					if (response.status == 200) {
						if (response.data.isCheck) {
							toastr.error('Số điện thoại này đã tồn tại', 'Quá trình kiểm tra thành công');
							return;
						} else {
							toastr.success('Có thể tạo tài khoản bằng số điện thoại này', 'Quá trình kiểm tra thành công');
							return;
						}
					} else {
						toastr.error('Quá trình kiểm tra bị lỗi', 'Lỗi nha');	
					}
				})
				.catch(error => {
					toastr.error('Quá trình kiểm tra bị lỗi', 'Lỗi nha');
				})
		}
	}
})